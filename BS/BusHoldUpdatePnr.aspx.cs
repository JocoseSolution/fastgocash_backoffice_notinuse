﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
//using PG;

public partial class BS_BusHoldUpdatePnr : System.Web.UI.Page
{
   

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        try
        {
            if (string.IsNullOrEmpty(Session["UID"].ToString()) | Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }

            if (IsPostBack)
            {
            }
            else
            {
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }
    }

    public void BindGrid()
    {
        try
        {
            GrdBusHoldReport.Columns[14].Visible = true;
            GrdBusHoldReport.Columns[15].Visible = true;
            GrdBusHoldReport.DataSource = BUSDetails();
            GrdBusHoldReport.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }

    public DataSet BUSDetails()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "USP_Bus_UpdatePnr";
                SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                da.Fill(DS);
               
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            
            DS.Dispose();
            con.Close();
        }
        return DS;
    }

    protected void ITZ_Accept_Click(object sender, System.EventArgs e)
    {
        try
        {
           
            dynamic OrderId = ((LinkButton)sender).CommandArgument.ToString();
            string popupScript = "window.open('HoldBusUpdate.aspx?OrderId=" + OrderId + "','Print','scrollbars=yes,width=800,height=500,top=20,left=150');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "popup", popupScript, true);
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }
    }

    protected void RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        try
        {
           if (e.CommandName == "Reject")
            {
                try
                {
                    //BindGrid();   
                    GrdBusHoldReport.Columns[14].Visible = false;
                    GrdBusHoldReport.Columns[15].Visible = false;
                    LinkButton lb = e.CommandSource as LinkButton;
                    GridViewRow gvr = lb.Parent.Parent as GridViewRow;
                    int RowIndex = gvr.RowIndex;
                    ViewState["RowIndex"] = RowIndex;
                    TextBox txtRemark = (TextBox)GrdBusHoldReport.Rows[RowIndex].FindControl("txtRemark");
                    LinkButton lnkSubmit = (LinkButton)GrdBusHoldReport.Rows[RowIndex].FindControl("lnkSubmit");
                    LinkButton lnkHides = (LinkButton)GrdBusHoldReport.Rows[RowIndex].FindControl("lnkHides");
                    lnkHides.Visible = true;
                    txtRemark.Visible = true;
                    lnkSubmit.Visible = true;
                    gvr.BackColor = System.Drawing.Color.Yellow;
                }
                catch (Exception ex)
                {
                    clsErrorLog.LogInfo(ex);

                }

            }
            else if (e.CommandName == "lnkHides")
            {
                try
                {

                    
                    LinkButton lb = e.CommandSource as LinkButton;
                    GridViewRow gvr = lb.Parent.Parent as GridViewRow;
                    int RowIndex = gvr.RowIndex;
                    ViewState["RowIndex"] = RowIndex;
                    TextBox txtRemark = (TextBox)GrdBusHoldReport.Rows[RowIndex].FindControl("txtRemark");
                    LinkButton lnkSubmit = (LinkButton)GrdBusHoldReport.Rows[RowIndex].FindControl("lnkSubmit");
                    LinkButton lnkHides = (LinkButton)GrdBusHoldReport.Rows[RowIndex].FindControl("lnkHides");
                    lnkHides.Visible = false;
                    txtRemark.Visible = false;
                    lnkSubmit.Visible = false;
                    gvr.BackColor = System.Drawing.Color.White;

                }
                catch (Exception ex)
                {
                    clsErrorLog.LogInfo(ex);

                }

            }
            else if (e.CommandName == "submit")
            {
              
                LinkButton lb = e.CommandSource as LinkButton;
                GridViewRow gvr = lb.Parent.Parent as GridViewRow;
                int RowIndex = gvr.RowIndex;
                ViewState["RowIndex"] = RowIndex;
                TextBox txtRemark = (TextBox)GrdBusHoldReport.Rows[RowIndex].FindControl("txtRemark");
                if (txtRemark.Text == "")
                {
                    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('please Provide Remark.');javascript: window.opener.location=window.opener.location.href;", true);
                }
                else
                {
                    try
                    {
                        int i = 0;
                        ViewState["OrderId"] = e.CommandArgument.ToString();
                        Label seatno = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("lbl_SEATNO");
                        LinkButton lnkSubmit = (LinkButton)GrdBusHoldReport.Rows[RowIndex].FindControl("lnkSubmit");
                        LinkButton lnkHides = (LinkButton)GrdBusHoldReport.Rows[RowIndex].FindControl("lnkHides");
                        lnkHides.Visible = false;
                        txtRemark.Visible = false;
                        lnkSubmit.Visible = false;
                        gvr.BackColor = System.Drawing.Color.White;                     
                        string Agent = Session["UID"].ToString();
                        Label lbl_Agent_ID = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("lbl_Agent_ID");
                        Label lbl_ORDERID = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("lbl_ORDERID");
                        Label refundamt = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("lbl_TA_NET_FARE");
                        Label bookamt = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("lbl_TA_NET_FARE");
                        Label refno = (Label)GrdBusHoldReport.Rows[RowIndex].FindControl("lbl_BOOKING_REF");
                        Boolean result = AutoRefund(lbl_ORDERID.Text, Convert.ToDouble(refundamt.Text), lbl_Agent_ID.Text, bookamt.Text, refno.Text);
                        if(result==true)
                        {
                         UpdateBuscanceldetail(Convert.ToString(ViewState["OrderId"]), Convert.ToInt32(seatno.Text), "B_R", "Rejected", txtRemark.Text);
                         ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Rejected Successfully.');javascript: window.close();window.opener.location=window.opener.location.href;", true);                       
                         BindGrid();
                        }
                        else
                        {
                         ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Try Again.');javascript: window.close();window.opener.location=window.opener.location.href;", true);
                         BindGrid();

                        }
                        GrdBusHoldReport.Columns[16].Visible = true;
                    }
                    catch (Exception ex)
                    {
                        clsErrorLog.LogInfo(ex);
                    }
                }

            }          
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }
    }

    protected void lnkHides_Click(object sender, System.EventArgs e)
    {
        try
        {
            GrdBusHoldReport.Columns[14].Visible = true;
            GrdBusHoldReport.Columns[15].Visible = true;
           
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }

    }

    public int UpdateBuscanceldetail(string orderid, int seatno, string ReqType, string Status, string remark)
    {
        int i = 0;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "USP_BusAccept_Rejecttkt";
                sqlcmd.Parameters.AddWithValue("@orderid", orderid);
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                sqlcmd.Parameters.AddWithValue("@remark", remark);
                sqlcmd.Parameters.AddWithValue("@seatno", seatno);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                i = sqlcmd.ExecuteNonQuery();
                

            }
        }

        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            con.Close();
        }
        return i;

    }    
    protected bool AutoRefund(string orderid, double RefundAmount, string AgentID, string BookingAmount, string refid)
    {
        
        bool Rfndstatus = false;      
        SqlTransaction ST = new SqlTransaction();
        SqlTransactionDom STDom = new SqlTransactionDom();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        try
        {
            DataSet AgencyDS = new DataSet();
            AgencyDS = ST.GetAgencyDetails(AgentID);                       
            RandomKeyGenerator rndnum = new RandomKeyGenerator();
            string numRand = rndnum.Generate();
            try
            {
                if (!string.IsNullOrEmpty(AgentID) && !string.IsNullOrEmpty(Convert.ToString(RefundAmount)))
                {
                    Rfndstatus = true;
                    double Aval_Bal = ST.AddCrdLimit(AgentID, RefundAmount);
                    STDom.insertLedgerDetails(AgentID, AgencyDS.Tables[0].Rows[0]["Agency_Name"].ToString(), orderid, refid, "", "", "", "", Session["UID"].ToString(), Request.UserHostAddress,
                    0, RefundAmount, Aval_Bal, "Bus booking Auto Rejection", "Refund Against  orderid=" + orderid, 0);                    
                }
            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);
            }
           }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        return Rfndstatus;
    }  
}