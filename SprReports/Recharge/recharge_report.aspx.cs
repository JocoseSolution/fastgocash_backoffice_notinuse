﻿using InstantPayServiceLib;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Recharge_recharge_report : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlDataAdapter adap;
    protected SqlTransactionDom STDom = new SqlTransactionDom();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        if (!this.IsPostBack)
        {
            BindDMTTranctionReport();
        }
    }

    protected void BindDMTTranctionReport()
    {
        DataTable dtRecord = GetRechargeTrancReportList(null, null, null, null, null, null);
        BindListView(dtRecord);
    }

    private void BindListView(DataTable dtRecord)
    {
        if (dtRecord != null && dtRecord.Rows.Count > 0)
        {
            Session["TransRecord"] = dtRecord;
            PagingButtom.Visible = true;
        }
        else
        {
            Session["TransRecord"] = null;
            PagingButtom.Visible = false;
        }

        lstTransaction.DataSource = dtRecord;
        lstTransaction.DataBind();
    }

    protected void btn_result_Click(object sender, EventArgs e)
    {
        string status = ddlstatus.SelectedValue;
        string transtype = ddltransType.SelectedValue;
        BindListView(GetRechargeTrancReportList(Request["hidtxtAgencyName"].ToString(), Request["From"].ToString(), Request["To"].ToString(), txt_Trackid.Text, transtype, status));
    }
    protected void lstTransaction_PagePropertiesChanged(object sender, EventArgs e)
    {
        string status = ddlstatus.SelectedValue;
        BindListView(Session["TransRecord"] as DataTable);
    }
    protected void btn_export_Click(object sender, EventArgs e)
    {
        string status = ddlstatus.SelectedValue;
        string transtype = ddltransType.SelectedValue;

        DataTable dtRecord = new DataTable();
        if (Session["TransRecord"] != null && string.IsNullOrEmpty(Request["From"].ToString()) && string.IsNullOrEmpty(Request["To"].ToString()))
        {
            dtRecord = Session["TransRecord"] as DataTable;
        }
        else
        {
            dtRecord = GetRechargeTrancReportList(Request["From"].ToString(), Request["To"].ToString(), txt_Trackid.Text, transtype, status);
        }
        dtRecord = GetDataorExport(dtRecord);
        DataSet dsExport = new DataSet();
        dsExport.Tables.Add(dtRecord);
        STDom.ExportData(dsExport, ("Recharge_Bil_Trans_Report_" + DateTime.Now));
    }

    private DataTable GetRechargeTrancReportList(string agencyid, string fromDate = null, string toDate = null, string trackid = null, string transtype = null, string status = null)
    {
        DataTable dtDueReport = new DataTable();

        try
        {
            string newfromdate = string.Empty; string newtodate = string.Empty;

            if (!string.IsNullOrEmpty(fromDate))
            {
                newfromdate = String.Format(fromDate.Split('-')[2], 4) + "-" + String.Format(fromDate.Split('-')[1], 2) + "-" + String.Format(fromDate.Split('-')[0], 2);
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                newtodate = String.Format(fromDate.Split('-')[2], 4) + "-" + String.Format(fromDate.Split('-')[1], 2) + "-" + String.Format(fromDate.Split('-')[0], 2);
            }

            adap = new SqlDataAdapter("sp_GetRchargeTransDetails", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@AgentId", !string.IsNullOrEmpty(agencyid) ? agencyid.Trim() : null);
            adap.SelectCommand.Parameters.AddWithValue("@ClientRefId", !string.IsNullOrEmpty(trackid) ? trackid.Trim() : null);
            adap.SelectCommand.Parameters.AddWithValue("@TransType", !string.IsNullOrEmpty(transtype) ? transtype.ToUpper().Trim() : null);
            adap.SelectCommand.Parameters.AddWithValue("@FromDate", !string.IsNullOrEmpty(newfromdate) ? newfromdate.Trim() : "");
            adap.SelectCommand.Parameters.AddWithValue("@ToDate", !string.IsNullOrEmpty(newtodate) ? newtodate.Trim() + " 23:59:59" : null);
            adap.SelectCommand.Parameters.AddWithValue("@Status", !string.IsNullOrEmpty(status) ? status.Trim() : null);
            adap.Fill(dtDueReport);

        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return dtDueReport;
    }

    private DataTable GetDataorExport(DataTable dtTrans)
    {
        DataTable mytable = new DataTable();

        try
        {
            mytable.Columns.Add("AgentId", typeof(string));
            mytable.Columns.Add("ClientRef Id", typeof(string));
            mytable.Columns.Add("Number", typeof(string));
            mytable.Columns.Add("ServiceType", typeof(string));
            mytable.Columns.Add("Operator", typeof(string));
            mytable.Columns.Add("Amount", typeof(string));
            mytable.Columns.Add("OrderID", typeof(string));
            mytable.Columns.Add("Status", typeof(string));
            mytable.Columns.Add("Trans Date", typeof(string));

            if (dtTrans != null && dtTrans.Rows.Count > 0)
            {
                for (int i = 0; i < dtTrans.Rows.Count; i++)
                {
                    DataRow dr1 = mytable.NewRow();
                    dr1 = mytable.NewRow();
                    dr1["AgentId"] = dtTrans.Rows[i]["Agentid"].ToString();
                    dr1["ClientRef Id"] = dtTrans.Rows[i]["ClientRefId"].ToString();
                    dr1["Number"] = dtTrans.Rows[i]["Number"].ToString();
                    dr1["ServiceType"] = dtTrans.Rows[i]["ServiceType"].ToString();
                    dr1["Operator"] = dtTrans.Rows[i]["Operator"].ToString();
                    dr1["Amount"] = dtTrans.Rows[i]["Amount"].ToString();
                    dr1["OrderID"] = dtTrans.Rows[i]["TransactionId"].ToString();
                    dr1["Status"] = dtTrans.Rows[i]["Status"].ToString();
                    dr1["Trans Date"] = dtTrans.Rows[i]["UpdatedDate"].ToString();
                    mytable.Rows.Add(dr1);
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return mytable;
    }

    [WebMethod]
    public static List<string> RefundUnderProcessStatus(string agentid, string crefid, string transid)
    {
        List<string> result = new List<string>();
        try
        {
            if (!string.IsNullOrEmpty(crefid))
            {
                DataTable dtTrans = SMBPApiService.GetBillTransactionHistory(agentid, null, null, crefid, "", "");
                if (dtTrans != null && dtTrans.Rows.Count > 0)
                {
                    string commAmt = !string.IsNullOrEmpty(dtTrans.Rows[0]["CommAmt"].ToString()) ? dtTrans.Rows[0]["CommAmt"].ToString() : "0";
                    string tds = !string.IsNullOrEmpty(dtTrans.Rows[0]["TDS"].ToString()) ? dtTrans.Rows[0]["TDS"].ToString() : "0";
                    string ledAmt = !string.IsNullOrEmpty(dtTrans.Rows[0]["LedAmt"].ToString()) ? dtTrans.Rows[0]["LedAmt"].ToString() : "0";
                    string agentId = !string.IsNullOrEmpty(dtTrans.Rows[0]["AgentId"].ToString()) ? dtTrans.Rows[0]["AgentId"].ToString() : string.Empty;
                    string billid = !string.IsNullOrEmpty(dtTrans.Rows[0]["BBPSPaymentId"].ToString()) ? dtTrans.Rows[0]["BBPSPaymentId"].ToString() : string.Empty;
                    string amount = !string.IsNullOrEmpty(dtTrans.Rows[0]["Amount"].ToString()) ? dtTrans.Rows[0]["Amount"].ToString() : string.Empty;

                    string deductAmt = Convert.ToDecimal(ledAmt) > 0 ? ledAmt : amount;
                    if (Convert.ToDecimal(deductAmt) > 0)
                    {
                        string agencyName = string.Empty; string agencyCreditLimit = string.Empty;
                        DataTable dtAgency = InstantPay_ApiService.GetAgencyDetailById(agentId);

                        if (dtAgency != null && dtAgency.Rows.Count > 0)
                        {
                            agencyName = dtAgency.Rows[0]["Agency_Name"].ToString();
                            agencyCreditLimit = dtAgency.Rows[0]["Crd_Limit"].ToString();

                            string callBack = SMBPApiService.BBPSPaymentEnquiry(agentId, crefid, transid, "under process");
                            if (!string.IsNullOrEmpty(callBack))
                            {
                                JObject dyResult = JObject.Parse(callBack);

                                string responseCode = (string)dyResult["ResponseCode"];
                                string responseMessage = (string)dyResult["ResponseMessage"];
                                string transactionId = (string)dyResult["TransactionId"];
                                string transactionStatus = (string)dyResult["TransactionStatus"];
                                string clientRefId = (string)dyResult["ClientRefId"];
                                string operatorTransactionId = (string)dyResult["OperatorTransactionId"];

                                if (responseCode == "000")
                                {
                                    if (transactionStatus.ToLower() == "success")
                                    {
                                        result.Add(responseCode);
                                        result.Add(responseMessage);
                                        result.Add(transactionStatus);
                                        result.Add("reload");
                                    }
                                    else if (transactionStatus.ToLower().Contains("fail"))
                                    {
                                        result.Add(responseCode);
                                        result.Add(responseMessage);
                                        result.Add(transactionStatus);
                                        result.Add("reload");

                                        List<string> isLedger = LedgerService.LedgerDebitCreditUtility(Math.Abs(Convert.ToDouble(deductAmt)), agentId, agencyName, agentId, "", 0, Convert.ToDouble(deductAmt), "Recharge", "Refund Recharge Amount", "CR", "Recharge", ("Refund_" + deductAmt + "_Recharge_By_Admin"), "CREDIT NOTE");

                                        if (isLedger.Count > 0)
                                        {
                                            string avlBal = isLedger[1];
                                            string trackid = isLedger[0];
                                            bool isSuccess = SMBPApiService.UpdateBSSPPaymentByStatus(billid, "Transaction Failed");
                                        }
                                    }
                                    else
                                    {
                                        result.Add(responseMessage);
                                        result.Add(transactionStatus);
                                    }
                                }
                                else if (responseCode == "999")
                                {
                                    result.Add(responseCode);
                                    result.Add(responseMessage);
                                    result.Add(transactionStatus);
                                }
                                else
                                {
                                    result.Add("Error Occurred !");
                                    result.Add(responseMessage);
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
            throw;
        }
        return result;
    }
}