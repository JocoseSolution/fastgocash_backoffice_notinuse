﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Admin_BookingBlock : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlDataAdapter adap;
    string msgout = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            if (string.IsNullOrEmpty(Convert.ToString(Session["User_Type"])))
            {
                Response.Redirect("~/Login.aspx");
            }
            //if (Convert.ToString(Session["User_Type"]).ToUpper() != "ADMIN")
            //{
            //    Response.Redirect("~/Login.aspx");
            //}      

            if (!IsPostBack)
            {
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }
    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            #region Insert
            //string Provider = Convert.ToString(DdlProvider.SelectedValue);
            //if (Provider == "0")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select provider!!');", true);
            //    return;
            //}
            string Airline = Convert.ToString(Request["hidtxtAirline"]);
            string FlightName = Convert.ToString(Request["txtAirline"]);
            string AirCode = "";
            string AirlineName = "";

            if (!string.IsNullOrEmpty(FlightName))
            {
                if (!string.IsNullOrEmpty(Airline))
                {
                    AirlineName = Airline.Split(',')[0];
                    if (Airline.Split(',').Length > 1)
                    {
                        AirCode = Airline.Split(',')[1];
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select again airline!!');", true);
                        return;
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select airline!!');", true);
                return;
                //AirlineName = "ALL";
                //AirCode = "ALL";
            }
            string Trip = Convert.ToString(DdlTripType.SelectedValue);
            //string TripTypeName = Convert.ToString(DdlTripType.SelectedItem.Text);
            string Provider = "";
           string FareType = Convert.ToString(DdlCrdType.SelectedValue);
            string FlightNumber = "";
            string Sector = "";
            string FromCity = "";
            string ToCity = "";
            string AgentType = "";
            string AgentId = "";
            string Status = Convert.ToString(DdlStatus.SelectedValue);            
            string ActionType = "insert";            
            msgout = "";
            int flag = AirBookingBlockByCompany(0, Trip, Provider, FareType, AirCode, FlightNumber, Sector, FromCity,ToCity, AgentType, AgentId, Status, ActionType);

            if (flag > 0)
            {                
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Added successfully.');window.location='DealCodeMaster.aspx'; ", true);                
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Added successfully.');", true);
                BindGrid();
            }
            else
            {
                if (msgout == "EXISTS")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Already exists,Please update..');", true);
                    BindGrid();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again.');", true);
                }

            }
            #endregion

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='TicketingCrd.aspx'; ", true);
            return;
        }

    }
    private int AirBookingBlockByCompany(int Counter, string Trip, string Provider, string FareType, string AirlineCode,string FlightNumber, string Sector, string FromCity, string ToCity, string AgentType, string AgentId, string Status,string ActionType)
    {        
        int flag = 0;
        string CreatedBy = Convert.ToString(Session["UID"]);
        //string ActionType = "insert";
        try
        {
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SP_T_AirBookingHold", con);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Counter", Counter);
            cmd.Parameters.AddWithValue("@Trip", Trip);
            cmd.Parameters.AddWithValue("@Provider", Provider);
            cmd.Parameters.AddWithValue("@FareType", FareType);
            cmd.Parameters.AddWithValue("@AirlineCode", AirlineCode);
            cmd.Parameters.AddWithValue("@FlightNumber", FlightNumber);
            cmd.Parameters.AddWithValue("@Sector", Sector);           
            cmd.Parameters.AddWithValue("@FromCity", FromCity);
            cmd.Parameters.AddWithValue("@ToCity", ToCity);
            // Counter, Trip, Provider, FareType, AirlineCode, FlightNumber, Sector, FromCity, ToCity, AgentType, 
            //AgentId, Status
            cmd.Parameters.AddWithValue("@AgentType", AgentType);           
            cmd.Parameters.AddWithValue("@AgentId", AgentId);
            cmd.Parameters.AddWithValue("@Status", Convert.ToBoolean(Status));            
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@ActionType", ActionType);
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == System.Data.ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
            msgout = cmd.Parameters["@Msg"].Value.ToString();

        }
        catch (Exception ex)
        {
            con.Close();
            clsErrorLog.LogInfo(ex);
        }
        return flag;

    }

    public void BindGrid()
    {
        try
        {
            Grid1.DataSource = GetRecord();
            Grid1.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    public DataTable GetRecord()
    {
        DataTable dt = new DataTable();
        try
        {
            string TripType = Convert.ToString(DdlTripType.SelectedValue);
            adap = new SqlDataAdapter("SP_T_AirBookingHold", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", "select");
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            adap.Dispose();
        }
        return dt;
    }

    protected void Grid1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            Grid1.EditIndex = e.NewEditIndex;
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }

    protected void Grid1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            Grid1.EditIndex = -1;
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void Grid1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int result = 0;
            Label lblSNo = (Label)(Grid1.Rows[e.RowIndex].FindControl("lblId"));
            int Counter = Convert.ToInt32(lblSNo.Text.Trim().ToString());

            
            DropDownList ddlGrdStatus = (DropDownList)Grid1.Rows[e.RowIndex].FindControl("ddlGrdStatus");
            string Status = ddlGrdStatus.SelectedValue;
            if (string.IsNullOrEmpty(Status))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Select Status!');", true);
                return;
            }
            

           
            string CrdType = "";
            string ActionType = "GRIDUPDATE";
            //result = TicketingCredential(Counter, CrpId, USERID, PWD, PCC, QNO, "", OnlineTkt, "", Provider, TicketThrough, QueuePCC, QNOForAPI, ForceToHold, ActionType, CrdType);
            result = AirBookingBlockByCompany(Counter, "", "", "", "", "", "", "", "", "", "", Status, ActionType);
            if (result > 0)
            {
                Grid1.EditIndex = -1;
                BindGrid();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Record successfully updated.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('try again.');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('" + ex.Message + "');", true);
        }
    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int flag = 0;
            try
            {
                Label lblSNo = (Label)(Grid1.Rows[e.RowIndex].FindControl("lblId"));
                int Id = Convert.ToInt16(lblSNo.Text.Trim().ToString());
                SqlCommand cmd = new SqlCommand("SP_T_AirBookingHold", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Counter", Id);
                cmd.Parameters.AddWithValue("@ActionType", "delete");
                cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                flag = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (SqlException ex)
            {
                con.Close();
                clsErrorLog.LogInfo(ex);
            }
            BindGrid();
            if (flag > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Problem in deleting record.');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }


    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Grid1.PageIndex = e.NewPageIndex;
        this.BindGrid();
    }

}