﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;

public partial class SprReports_Accounts_PGCharges : System.Web.UI.Page
{
    DataTable dt = new DataTable();
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    #region New Concept Code
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    string msgout = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        DivMsg.InnerHtml = "";
        DivMsgExclued.InnerHtml = "";
        if (!string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            try
            {
                if (!IsPostBack)
                {
                    ddl_ptype.AppendDataBoundItems = true;
                    ddl_ptype.Items.Clear();
                    //Dim item As New ListItem("All Type", "0")
                    //ddl_ptype.Items.Insert(0, item)
                    ddl_ptype.DataSource = STDom.GetAllGroupType().Tables[0];
                    ddl_ptype.DataTextField = "GroupType";
                    ddl_ptype.DataValueField = "GroupType";
                    ddl_ptype.DataBind();
                    ddl_ptype.Items.Insert(0, new ListItem("-- Select Type --", "ALL"));
                    BindGrid();
                }


            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);
            }
        }
        else
        {
            Response.Redirect("~/Login.aspx");
        }
    }


    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        #region Insert
        try
        {           
            int Id = 0;
            string PaymentCode = ddlCardType.SelectedValue;
            string PaymentMode = ddlCardType.SelectedItem.Text;
            string Charges = txtPgCharges.Text.Trim();
            string ChargesType = ddlChargeType.SelectedValue;
            string GroupType= ddl_ptype.SelectedValue;
            string AgentId = "";// Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
            string SelectedUserId = hfCustomerId.Value;
            string ActionType = "insert";
            string AddMsg = "";
            msgout = "";
            DivMsg.InnerHtml = "";
            if (!string.IsNullOrEmpty(SelectedUserId))
                {
                 #region Insert PG Charges Agent UserId Wise
                int flag = 0;
                    int len = SelectedUserId.Split(',').Length;
                    for (int i = 0; i < len - 1; i++)
                    {
                        msgout = "";
                        GroupType = "";
                        AgentId = SelectedUserId.Split(',')[i];
                        flag = InsertAndUpdate(Id, PaymentCode, PaymentMode, Charges, ChargesType, GroupType, AgentId, ActionType);                       
                        AddMsg = AddMsg + AgentId + "-" + msgout + ".<br />";
                    }
                    hfCustomerId.Value = "";
                    if (!string.IsNullOrEmpty(AddMsg))
                    {                      
                        DivMsg.InnerHtml = AddMsg;                      
                        hidActionType.Value = "select";
                        BindGrid();
                    }
                    else
                    {                       
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again');", true);
                        hidActionType.Value = "select";
                        BindGrid();
                    }
                #endregion Insert PG Charges AgentWise
                }
            else
                {
                 #region Insert PG Charges Agent Type Wise
                    DivMsg.InnerHtml = AddMsg;
                    AgentId = "";
                    msgout = "";                    
                    int flag = InsertAndUpdate(Id, PaymentCode, PaymentMode, Charges, ChargesType, GroupType, AgentId, ActionType);
                    AddMsg = msgout;                    
                    hfCustomerId.Value = "";                   
                    DivMsg.InnerHtml = AddMsg;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert(" + AddMsg + ");", true);                   
                    hidActionType.Value = "select";
                    BindGrid();
                #endregion Insert PG Charges Agent Type Wise
            }
            #endregion            
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='PGCharges.aspx'; ", true);
            return;
        }
        #endregion
    }

    private int InsertAndUpdate(int Id, string PaymentCode, string PaymentMode, string Charges, string ChargesType, string GroupType, string AgentId, string ActionType)
    {       
        int flag = 0;
        string ActionBy = Convert.ToString(Session["UID"]);

        Charges = Regex.Replace(Charges, @"\s+", "");
        if (string.IsNullOrEmpty(Regex.Replace(Charges, @"\s+", "")))
        {
            Charges = "0";
        }
            try
        {
            SqlCommand cmd = new SqlCommand("SP_PGTransChargeMaster", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", Id);
            cmd.Parameters.AddWithValue("@PaymentCode", PaymentCode);
            cmd.Parameters.AddWithValue("@PaymentMode", PaymentMode);            
            cmd.Parameters.AddWithValue("@Charges", Charges);
            cmd.Parameters.AddWithValue("@ChargesType", ChargesType);
            cmd.Parameters.AddWithValue("@GroupType", GroupType);
            cmd.Parameters.AddWithValue("@AgentId", AgentId);
            cmd.Parameters.AddWithValue("@ActionBy", ActionBy);
            cmd.Parameters.AddWithValue("@Action", ActionType);
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
            msgout = cmd.Parameters["@Msg"].Value.ToString();
        }
        catch (Exception ex)
        {
            con.Close();
            clsErrorLog.LogInfo(ex);
        }
        return flag;

    }
    public void BindGrid()
    {
        try
        {
            string PaymentCode = "", ChargesType = "", GroupType = "", AgentId = "";            
            #region

            if (hidActionType.Value == "search")
            {
                #region search value set 
                PaymentCode= ddlCardType.SelectedValue;
                ChargesType= ddlChargeType.SelectedValue;
                GroupType = ddl_ptype.SelectedValue;
                string SelectedUserId = hfCustomerId.Value;
               
                DivMsg.InnerHtml = "";

                if (!string.IsNullOrEmpty(SelectedUserId))
                {
                    string JoinAgentId = "";
                    int len = SelectedUserId.Split(',').Length;//> 1
                    for (int i = 0; i < len - 1; i++)
                    {
                        msgout = "";
                        GroupType = "";
                        //AgentId = SelectedUserId.Split(',')[i];
                        JoinAgentId = JoinAgentId + "'" + SelectedUserId.Split(',')[i] + "',";

                    }
                    JoinAgentId = JoinAgentId.TrimEnd(',');
                    AgentId = JoinAgentId; //"(" + JoinAgentId + ")";
                                           // hfCustomerId.Value = "";                   
                }
                else
                {
                    hfCustomerId.Value = "";
                }

                #endregion
            }
            #endregion
            
            grd_P_IntlDiscount.DataSource = GetRecord(PaymentCode, ChargesType, GroupType, AgentId);
            grd_P_IntlDiscount.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    public DataTable GetRecord(string PaymentCode, string ChargesType, string GroupType, string AgentId)
    {
        //Id, PaymentCode, PaymentMode, Charges, ChargesType, ValidTo, ValidFrom, Status, CreatedBy, CreatedDate, UpdatedDate, UpdatedBy, GroupType, 
        //                 UserId, AgencyId, AgencyName

        DataTable dt = new DataTable();
        try
        {
            string ActionType = hidActionType.Value;
            if (con.State == ConnectionState.Closed)
                con.Open();                   
            adap = new SqlDataAdapter("SP_PGTransChargeMaster", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@PaymentCode", PaymentCode);
            adap.SelectCommand.Parameters.AddWithValue("@ChargesType", ChargesType);
            adap.SelectCommand.Parameters.AddWithValue("@GroupType", GroupType);
            adap.SelectCommand.Parameters.AddWithValue("@AgentId", AgentId);
            adap.SelectCommand.Parameters.AddWithValue("@action", ActionType);
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
            adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            con.Close();
            adap.Dispose();
        }
        return dt;
    }


    protected void grd_P_IntlDiscount_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            grd_P_IntlDiscount.EditIndex = e.NewEditIndex;
            //ActionTypeGrid = "select";
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }

    protected void grd_P_IntlDiscount_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            grd_P_IntlDiscount.EditIndex = -1;
            //ActionTypeGrid = "select";
            BindGrid();

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void grd_P_IntlDiscount_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int result = 0;
            Label lblSNo = (Label)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("lblId"));
            int Id = Convert.ToInt32(lblSNo.Text.Trim().ToString());  
            TextBox txtChargesGrd = (TextBox)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("txtChargesGrd"));
            string ChargesGrd = Convert.ToString(txtChargesGrd.Text);
            DropDownList ddlChargesTypeGrd = (DropDownList)grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("ddlChargesTypeGrd");
            string ChargesTypeGrd = ddlChargesTypeGrd.SelectedValue;
            string ActionType = "update";
            if (!string.IsNullOrEmpty(Convert.ToString(Id))  && !string.IsNullOrEmpty(ChargesGrd) && !string.IsNullOrEmpty(ChargesTypeGrd))
            {
                result = InsertAndUpdate(Id, "", "", ChargesGrd, ChargesTypeGrd, "", "", ActionType);
                grd_P_IntlDiscount.EditIndex = -1;
                // ActionTypeGrid = "select";
                BindGrid();

                if (result > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Successfully updated.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('try again.');", true);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ChargesGrd))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Enter PG Charges.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Try again.');", true);
                }
            }
            
           
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('" + ex.Message + "');", true);
        }
    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            SqlCommand cmd=null;
            int flag = 0;
            string ActionBy = Convert.ToString(Session["UID"]);
            try
            {
                Label lblSNo = (Label)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("lblId"));
                int Id = Convert.ToInt16(lblSNo.Text.Trim().ToString());
                cmd = new SqlCommand("SP_PGTransChargeMaster", con);////SpSearchResultBlock
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@ActionBy", ActionBy);
                cmd.Parameters.AddWithValue("@Action", "delete");
                cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                flag = cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Close();
            }
            catch (SqlException ex)
            {
                con.Close();
                cmd.Dispose();               
                clsErrorLog.LogInfo(ex);
            }
            //ActionTypeGrid = "select";
            BindGrid();
            if (flag > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Problen in deleting record.');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }


    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grd_P_IntlDiscount.PageIndex = e.NewPageIndex;
        //ActionTypeGrid = "select";        
        this.BindGrid();
    }   
    protected void BtnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            hidActionType.Value = "search";
            BindGrid();


        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='PGCharges.aspx'; ", true);
            return;
        }

    }

   
    

    #region Old Page Concept Code
    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        if (Session["UID"] == null || string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
    //        {
    //            Response.Redirect("~/Login.aspx");
    //        }
    //        if (IsPostBack == false)
    //        {
    //            GetAllPaymentMode();
    //            BindGridView();
    //        }
    //    }
    //    catch (Exception ex)
    //    { 


    //    }
    //}
    //private void GetAllPaymentMode()
    //{
    //    try
    //    {
    //        DataTable dt = new DataTable();
    //        SqlDataAdapter da = new SqlDataAdapter("SP_PGTransCharge", con);
    //        da.SelectCommand.CommandType = CommandType.StoredProcedure;
    //        da.SelectCommand.Parameters.AddWithValue("@action", "All");
    //        da.Fill(dt);            
    //        ddlCardType.DataSource = dt;
    //        ddlCardType.DataValueField = "Id";
    //        ddlCardType.DataTextField = "PaymentMode";
    //        ddlCardType.DataBind();
    //        //Adding "select" option in dropdownlist 
    //        ddlCardType.Items.Insert(0, new ListItem("Select Payment Mode", "0"));
    //    }
    //    catch (Exception ex)
    //    {

    //        //con.Close();

    //    }
    //}

    //private void BindGridView()
    //{
    //    try
    //    {
    //        DataTable dt = new DataTable();
    //        SqlDataAdapter da = new SqlDataAdapter("SP_PGTransCharge", con);
    //        da.SelectCommand.CommandType = CommandType.StoredProcedure;
    //        da.SelectCommand.Parameters.AddWithValue("@action", "All");
    //        da.Fill(dt);
    //        GridView1.DataSource = dt;
    //        GridView1.DataBind();            
    //    }
    //    catch (Exception ex)
    //    {
    //        //con.Close();
    //    }
    //}
    //protected void ddlCardType_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        string Id = ddlCardType.SelectedValue;
    //        string PaymentMode = ddlCardType.SelectedItem.Text;
    //        GetPgTransDetailsByPgMode(Id, PaymentMode);
    //        //btnUpdate.Visible = false;            
    //    }
    //    catch (Exception ex)
    //    {
    //        Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message", "alert('" + Convert.ToString(ex.Message) + "');", true);
    //    }

    //}
    //private void GetPgTransDetailsByPgMode(string Id, string PaymentMode)
    //{
    //    try
    //    {
    //        DataTable dt = new DataTable();
    //        SqlDataAdapter da = new SqlDataAdapter("SP_PGTransCharge", con);
    //        da.SelectCommand.CommandType = CommandType.StoredProcedure;
    //        da.SelectCommand.Parameters.AddWithValue("@Id", Id);
    //        da.SelectCommand.Parameters.AddWithValue("@PaymentMode", PaymentMode);
    //        da.SelectCommand.Parameters.AddWithValue("@action", "GetDetails");
    //        da.Fill(dt);           
    //        if (dt.Rows.Count > 0)
    //        {
    //            ddlChargeType.SelectedValue = Convert.ToString(dt.Rows[0]["ChargesType"]).Trim();
    //            //txtPgCharges.Text = (Convert.ToDouble(dt.Rows[0]["Charges"])).ToString("0.00");
    //            txtPgCharges.Text = Convert.ToString(dt.Rows[0]["Charges"]).Trim();
    //        }
    //        else
    //        {                
    //            txtPgCharges.Text = "";
    //        }
    //    }
    //    catch (Exception ex)
    //    {            
    //        //con.Close();
    //    }
    //}


    //protected void btnUpdate_Click(object sender, EventArgs e)
    //{ 
    //    if (ddlCardType.SelectedValue == "0")
    //    {
    //        Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message", "alert('Please select payment mode');", true);
    //        return;
    //    }
    //    if (txtPgCharges.Text.Trim() == "")
    //    {
    //        Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message", "alert('Please enter transaction charges');", true);           
    //        return;
    //    }
    //    if (!String.IsNullOrEmpty(ddlCardType.SelectedValue) && ddlCardType.SelectedValue != "0" && !String.IsNullOrEmpty(txtPgCharges.Text))       
    //    {
    //        SqlCommand cmd = new SqlCommand("SP_PGTransCharge", con);
    //        cmd.CommandType = CommandType.StoredProcedure;
    //        try
    //        {
    //            cmd.Parameters.AddWithValue("@id", ddlCardType.SelectedValue);                
    //            cmd.Parameters.AddWithValue("@Charges",Convert.ToDouble(txtPgCharges.Text));
    //            cmd.Parameters.AddWithValue("@ChargesType", ddlChargeType.SelectedValue);
    //            cmd.Parameters.AddWithValue("@ActionBy", Convert.ToString(Session["UID"]));
    //            cmd.Parameters.AddWithValue("@action", "update");
    //            if (con.State == ConnectionState.Closed)
    //                con.Open();
    //            int flag = cmd.ExecuteNonQuery();
    //            if (flag > 0)
    //            {
    //                GetPgTransDetailsByPgMode(ddlCardType.SelectedValue, ddlCardType.SelectedItem.Text);
    //                BindGridView();
    //                Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message", "alert('Payment gateway transaction charges updated ');", true);
    //            }
    //            else
    //                Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message", "alert('Try again);", true);
    //        }
    //        catch (Exception ex)
    //        {
    //            if (con.State == ConnectionState.Open)
    //                con.Close();
    //            Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message", "alert('" + Convert.ToString(ex.Message) + "');", true);
    //        }
    //        finally
    //        {
    //            if (con.State == ConnectionState.Open)
    //                con.Close();
    //        }
    //    }
    //    else
    //    {
    //        Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message", "alert('Please again select payment mode,than update');", true);
    //    }
    //}

    #endregion   Old Page Concept Code
}