﻿<%@ Page Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="HotelSalesRegister.aspx.vb" Inherits="SprReports_Accounts_HotelSalesRegister" %>
<%@ Register Src="~/UserControl/HotelMenu.ascx" TagPrefix="uc1" TagName="HotelMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
     <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
    rel="stylesheet" />
       <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>

    <div class="row">
       <div class="col-md-12"  >
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Account > Search Hotel Sale Register </h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">From Date:</label>
                                    <input type="text" name="From" id="From" readonly="readonly" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">To Date:</label>
                                    <input type="text" name="To" id="To" readonly="readonly" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Order ID:</label>
                                    <asp:TextBox ID="txt_OrderId" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            </div>
                              <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">
                                        BookingID
                                    </label>
                                    <asp:TextBox ID="txt_bookingID" runat="server" CssClass="form-control"></asp:TextBox>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">
                                        Hotel Name</label>
                                    <asp:TextBox ID="txt_htlcode" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">
                                        Room Name
                                    </label>
                                    <asp:TextBox ID="txt_roomcode" runat="server" CssClass="form-control"></asp:TextBox>


                                </div>
                            </div>
                                  </div>

                                    <div class="row">
                            <div class="col-md-4" id="TDAgency" runat="server">
                                <div class="form-group" id="TDAgency1" runat="server">
                                    <label for="exampleInputPassword1">Agency Name</label>
                                    <input type="text" id="txtAgencyName" name="txtAgencyName" class="form-control" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" value="Agency Name or ID" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="button buttonBlue"  />
                                     
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                     <br />
                                    <asp:Button ID="btn_Export" runat="server" Text="Export" CssClass="button buttonBlue"  />
                                </div>
                            </div>

                            </div>


                            <div class="clear"></div>
                            <div class="large-8 medium-8 small-12 columns" style="font-size: 11px; line-height: 20px; text-align: justify; color: Red;">
                                * To get today booking, do not fill any field, only click on Search Result Button.
                            </div>                          
                            <div class="clear1"></div>                        
                    <div class="row">
                        <div class="col-md-4">
                                <div class="form-group">                                   
                            Total Amount : <asp:Label ID="lbl_Total" runat="server"></asp:Label>
                        </div>
                      </div>
                        <div class="col-md-4">
                        <div class="form-group">
                            Total Hotel Booked :
                                <asp:Label ID="lbl_counttkt" runat="server"></asp:Label>
                            </div>
                        </div>
                        </div>
                        <div class="clear1"></div>
                        <div class="row">
                        <asp:GridView ID="GrdReport" runat="server" AutoGenerateColumns="False" CssClass="table"
                             BorderStyle="None"  AllowPaging="True" PageSize="20">
                            <Columns>
                                <asp:TemplateField HeaderText="OrderID">
                                    <ItemTemplate>
                                        <a href='HotelInvoiceDetails.aspx?OrderId=<%#Eval("OrderId") %>&amp;AgentID=<%#Eval("AgentId") %>' rel="lyteframe"
                                            rev="width: 686px; height: 551px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">Invoice</a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="BookingID" HeaderText="BookingID" ReadOnly="true" />
                                <asp:BoundField DataField="AgencyId" HeaderText="AgencyId" />
                                <asp:BoundField DataField="Status" HeaderText="Status" />                                
                                <asp:BoundField DataField="AgencyName" HeaderText="Agency Name" />
                                <asp:BoundField DataField="PurchaseCost" HeaderText="Purchase Cost" />
                                <asp:BoundField DataField="NetCost" HeaderText="Net Cost" />
                                <asp:BoundField DataField="TotalCost" HeaderText="Total Cost" />
                                <asp:BoundField DataField="HotelName" HeaderText="Hotel Name" />
                                <asp:BoundField DataField="RoomName" HeaderText="Room Name" />
                                <asp:BoundField DataField="StarRating" HeaderText="Star" />
                                <asp:BoundField DataField="BookingDate" HeaderText="Booking Date" />
                                 <asp:BoundField DataField="Provider" HeaderText="Supplier" />
                                <asp:BoundField DataField="UserId" HeaderText="UserId" />    
                            </Columns>
                           <%-- <RowStyle BackColor="White" ForeColor="#003399" />
                            <FooterStyle BackColor="#003399" Font-Bold="True" ForeColor="#dbecf3" />
                            <PagerStyle BackColor="#003399" ForeColor="#dbecf3" HorizontalAlign="Left" CssClass="pgr" />
                            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                            <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#dbecf3" />--%>
                        </asp:GridView>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
                <script type="text/javascript">
                    var myDate = new Date();
                    var currDate = (myDate.getFullYear()) + '-' + (myDate.getMonth() + 1) + '-' + myDate.getDate();
                    //document.getElementById("From").value = currDate;
                    //document.getElementById("To").value = currDate;
                    var UrlBase = '<%=ResolveUrl("~/") %>';
                </script>
           <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

            <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

            <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>