﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="InstantPayLogList.aspx.cs" Inherits="SprReports_Money_Transfer_InstantPayLogList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/Hotel/css/B2Bhotelengine.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }

        .tooltip1 {
            position: relative;
        }

        .tooltiptext {
            visibility: hidden;
            width: 120px;
            background-color: black;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            /* Position the tooltip */
            position: absolute;
            z-index: 1;
        }

        #tooltip {
            z-index: 9999;
            position: absolute;
            top: 200px;
            float: right;
            padding: 5px;
            right: 280px;
            border: 2px solid #04034f;
            background-color: #fff;
            width: auto;
            min-width: 300px;
        }

        .table .table {
            background-color: #fff;
            border: 1px solid #ccc;
        }

        .tooltip1:hover .tooltiptext {
            visibility: visible;
        }

        .popupnew2 {
            position: absolute;
            top: 10px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .hovercolor {
            font-weight: bold;
            color: #004b91;
            font-size: 11px;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: auto !important;
            overflow-y: auto !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }
    </style>
    <style>
        .pagination {
            margin: 0;
            /*margin-bottom: 30px;*/
        }

            .pagination > span > a, .pagination > span > span {
                background-color: #fff;
                border: 1px solid #ddd;
                display: inline;
                float: left;
                line-height: 1.42857;
                margin-left: -1px;
                padding: 6px 12px;
                position: relative;
                text-decoration: none;
            }

            .pagination > span > a {
                color: #E7302A !important;
            }

            .pagination > span > span {
                color: #fff !important;
            }

                .pagination > span > a:first-child, .pagination > span > span:first-child {
                    border-bottom-left-radius: 4px;
                    border-top-left-radius: 4px;
                    margin-left: 0;
                    color: #E7302A !important;
                }

                .pagination > span > a:last-child, .pagination > span > span:last-child {
                    border-bottom-right-radius: 4px;
                    border-top-right-radius: 4px;
                    color: #E7302A !important;
                }

            .pagination > span > span {
                background-color: #E7302A !important;
                color: #FFF;
                cursor: default;
                z-index: 2;
            }

            .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
                z-index: 3;
                color: #fff;
                cursor: default;
                background-color: #00c2ff !important;
                border-color: #00c2ff !important;
                cursor: pointer;
            }

            .pagination > li > a, .pagination > li > span {
                cursor: pointer;
            }

        iframe {
            height: 500px !important;
        }
    </style>
    <div class="row">
        <div class="page-wrapperss">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Transaction Report</h3>
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="row form-group">                            
                            <div class="col-md-3">
                                <label>From</label>
                                <input type="text" name="From" id="From" placeholder="Select Date" class="form-control" readonly="readonly" />
                            </div>
                            <div class="col-md-3">
                                <label>To</label>
                                <input type="text" name="To" placeholder="Select Date" id="To" class="form-control" readonly="readonly" />
                            </div>
                            <div class="col-md-3">
                                <label>TrackID</label>
                                <asp:TextBox ID="txt_Trackid" placeholder="Enter Track Id" class="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <br />
                                <asp:Button ID="btn_result" runat="server" class="button buttonBlue" Text="Search Result" OnClick="btn_result_Click"/>
                            </div>
                        </div>
                        <div class="row form-group" style="font-size: 15px; line-height: 20px; text-align: justify; color: Red;">
                            * N.B: To get Today's transaction without above parameter,do not fill any field, only click on search your transaction.
                        </div>
                        <%--<div class="row form-group">
                            <div class="col-sm-3">
                                <br />
                                <asp:Button ID="btn_export" runat="server" CssClass="button buttonBlue" Text="Export"  OnClick="btn_export_Click"/>                            
                            </div>
                        </div>--%>
                        <div class="row form-group">
                            <div class="col-md-12 form-group">
                                <div class="row table-responsive text-nowrap">
                                    <table class="table" data-toggle="table" style="width: 100%!important">
                                        <asp:ListView ID="lstTransaction" runat="server" OnPagePropertiesChanged="lstTransaction_PagePropertiesChanged">
                                            <LayoutTemplate>
                                                <tr>
                                                    <th>Detail</th>
                                                    <th>TxnDate</th>
                                                    <th>AgentId</th>
                                                    <th>Agency_Name</th>
                                                    <th>TrackId</th>
                                                    <th>PostUrl</th>
                                                    <th>RequestJson</th>
                                                    <th>ResponseJson</th>
                                                    <th>ActionType</th>
                                                </tr>
                                                <div runat="server" id="ItemPlaceholder"></div>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <%--<%# Eval("BookingType").ToString().Trim() != "Beneficiary_Name" ? "<a target='_blank' href='GetFundDetailByTrackId.aspx?TrackId=" + Eval("TrackId") +"' rel='lyteframe' rev='width: 900px; height: 280px; overflow:hidden;' target='_blank' style='color: #fff; font-weight: bold; border: 1px solid red; padding: 2px; background: red; cursor: pointer;'>Detail</a>" : "- - -" %>--%>
                                                        <a target='_blank' href="GetFundDetailByTrackId.aspx?TrackId=<%#Eval("TrackId") %>" rel='lyteframe' rev='width: 900px; height: 280px; overflow:hidden;' target='_blank' style='color: #fff; font-weight: bold; border: 1px solid red; padding: 2px; background: red; cursor: pointer;'>Detail</a>
                                                    </td>
                                                    <td><%# Eval("CreatedDate") %></td>
                                                    <td><%# Eval("AgentId") %></td>
                                                    <td><%# Eval("Agency_Name") %></td>
                                                    <td><%# Eval("TrackId") %></td>                                                   
                                                     <td>                                                        
                                                        <span id="posturl_<%# Eval("ResponseId") %>" style="display: none;"><%# Eval("PostUrl") %></span>
                                                        <span onclick="JSONPostUrlViewer(<%# Eval("ResponseId") %>)" style="color: #fff; font-weight: bold; border: 1px solid red; padding: 2px; background: red; cursor: pointer;">View Hit URL</span>
                                                    </td>
                                                    <td>                                                        
                                                        <span id="jsonreqview_<%# Eval("ResponseId") %>" style="display: none;"><%# Eval("RequestJson") %></span>
                                                        <span onclick="JSONReqViewer(<%# Eval("ResponseId") %>)" style="color: #fff; font-weight: bold; border: 1px solid red; padding: 2px; background: red; cursor: pointer;">View Request</span>
                                                    </td>
                                                    <td>
                                                        <span id="jsonresview_<%# Eval("ResponseId") %>" style="display: none;"><%# Eval("ResponseJson") %></span>
                                                        <span onclick="JSONResViewer(<%# Eval("ResponseId") %>)" style="color: #fff; font-weight: bold; border: 1px solid red; padding: 2px; background: red; cursor: pointer;">View API Response</span>
                                                    </td>
                                                    <%--<td><%# Eval("ActionType") %></td>--%>
                                                    <td><%# Eval("BookingType") %></td>
                                                </tr>
                                            </ItemTemplate>
                                            <EmptyDataTemplate>
                                                <table class="table" data-toggle="table" style="width: 100%!important">
                                                    <tr>
                                                        <th>TxnDate</th>
                                                        <th>AgentId</th>
                                                        <th>Agency_Name</th>
                                                        <th>TrackId</th>
                                                        <th>PostUrl</th>
                                                        <th>RequestJson</th>
                                                        <th>ResponseJson</th>
                                                        <th>ActionType</th>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="8" style="color: red; text-align: center;">NO RECORD FOUND !</td>
                                                    </tr>
                                                </table>
                                            </EmptyDataTemplate>
                                        </asp:ListView>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <ul class="pagination">
                                    <asp:DataPager runat="server" ID="PagingButtom" PagedControlID="lstTransaction" PageSize="15">
                                        <Fields>
                                            <asp:NextPreviousPagerField FirstPageText="First" PreviousPageText="&laquo;" ShowFirstPageButton="false" ShowNextPageButton="false" ShowPreviousPageButton="true" RenderDisabledButtonsAsLabels="false" />
                                            <asp:NumericPagerField ButtonCount="5" />
                                            <asp:NextPreviousPagerField LastPageText="Last" NextPageText="&raquo;" ShowLastPageButton="false" ShowNextPageButton="true" ShowPreviousPageButton="false" RenderDisabledButtonsAsLabels="false" />
                                        </Fields>
                                    </asp:DataPager>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
   
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript">
        $(function () {
            InitializeToolTip();
        });

        function JSONReqViewer(jsonstr) {           
            alert($("#jsonreqview_" + jsonstr).html());
        }

        function JSONResViewer(jsonstr) {           
            alert($("#jsonresview_" + jsonstr).html());
        }

        function JSONPostUrlViewer(jsonstr) {           
            alert($("#posturl_" + jsonstr).html());
        }
    </script>
</asp:Content>

