﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Money_Transfer_GetFundDetailByTrackId : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlDataAdapter adap;
    protected SqlTransactionDom STDom = new SqlTransactionDom();

    public string td_ipay_id_str = string.Empty; public string td_opr_id_str = string.Empty; public string reqAmt = string.Empty; public string chAmt = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        if (!this.IsPostBack)
        {
            string TrackId = Request.QueryString["TrackId"] != null ? Request.QueryString["TrackId"].ToString() : string.Empty;

            if (!string.IsNullOrEmpty(TrackId))
            {
                DataTable dtDueReport = new DataTable();

                try
                {
                    adap = new SqlDataAdapter("sp_GetInstantPayFundTransferLogByTrackId", con);
                    adap.SelectCommand.CommandType = CommandType.StoredProcedure;
                    adap.SelectCommand.Parameters.AddWithValue("@TrackId", TrackId);
                    adap.Fill(dtDueReport);
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }

                if (dtDueReport != null && dtDueReport.Rows.Count > 0)
                {
                    int count = 0; /*string td_ipay_id_str = string.Empty; string td_opr_id_str = string.Empty; string reqAmt = string.Empty; string chAmt = string.Empty;*/
                    for (int i = 0; i < dtDueReport.Rows.Count; i++)
                    {
                        if (count == 0)
                        {
                            td_TxnDate.InnerText = dtDueReport.Rows[0]["CreatedDate"].ToString();
                            td_TrackId.InnerText = dtDueReport.Rows[0]["TrackId"].ToString();
                            td_AgentId.InnerText = dtDueReport.Rows[0]["AgentId"].ToString();
                            td_Agency_Name.InnerText = dtDueReport.Rows[0]["Agency_Name"].ToString();

                            td_RemitterMobile.InnerText = dtDueReport.Rows[0]["RemitterMobile"].ToString();
                            td_BenificieryId.InnerText = dtDueReport.Rows[0]["BenificieryId"].ToString();
                            td_BenificieryName.InnerText = dtDueReport.Rows[0]["name"].ToString();

                            td_bank_alias.InnerText = dtDueReport.Rows[0]["bank_alias"].ToString();

                            string mode = dtDueReport.Rows[0]["TxnMode"].ToString();

                            td_TxnMode.InnerText = (mode == "DPN" ? "IMPS" : (mode == "BPN" ? "NEFT" : (mode == "CPN" ? "RTGS" : mode)));
                            td_Status.InnerText = dtDueReport.Rows[0]["Status"].ToString();
                            td_ledDebit.InnerText = "₹" + dtDueReport.Rows[0]["Debit"].ToString();
                        }

                        td_ipay_id_str = !string.IsNullOrEmpty(td_ipay_id_str) ? td_ipay_id_str + "<p>" + dtDueReport.Rows[i]["ipay_id"].ToString() + "<p/>" : "<p>" + dtDueReport.Rows[i]["ipay_id"].ToString() + "<p/>";
                        td_opr_id_str = !string.IsNullOrEmpty(td_opr_id_str) ? td_opr_id_str + "<p>" + dtDueReport.Rows[i]["opr_id"].ToString() + "<p/>" : "<p>" + dtDueReport.Rows[i]["opr_id"].ToString() + "<p/>";

                        reqAmt = !string.IsNullOrEmpty(reqAmt) ? reqAmt + "<p>₹ " + dtDueReport.Rows[i]["Amount"].ToString() + "<p/>" : "<p>₹ " + dtDueReport.Rows[i]["Amount"].ToString() + "<p/>";
                        chAmt = !string.IsNullOrEmpty(chAmt) ? chAmt + "<p>₹ " + dtDueReport.Rows[i]["charged_amt"].ToString() + "<p/>" : "<p>₹ " + dtDueReport.Rows[i]["charged_amt"].ToString() + "<p/>";

                        count = count + 1;
                    }
                    //td_ipay_id.InnerText = td_ipay_id_str;
                    //td_opr_id.InnerText = td_opr_id_str;
                    //td_RequestAmount.InnerText = reqAmt;
                    //td_Charged_Amount.InnerText = chAmt;


                }
            }
        }
    }
}