﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Money_Transfer_UtilityCommission : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            Response.Redirect("~/Login.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                BindGroupType();
                BindUtilityService();
                BindGridview();
            }
        }
    }

    private void BindGroupType()
    {
        try
        {
            ddlGroupType.Items.Clear();

            ddlGroupType.DataSource = STDom.GetAllGroupType().Tables[0];
            ddlGroupType.DataTextField = "GroupType";
            ddlGroupType.DataValueField = "GroupType";
            ddlGroupType.DataBind();
            ddlGroupType.Items.Insert(0, new ListItem("ALL", ""));
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    private void BindUtilityService()
    {
        try
        {
            ddlUtilityService.Items.Clear();

            ddlUtilityService.DataSource = STDom.GetSMBP_BBPSBillerList("", "biller").Tables[0];
            ddlUtilityService.DataTextField = "ServiceType";
            ddlUtilityService.DataValueField = "ServiceType";
            ddlUtilityService.DataBind();
            ddlUtilityService.Items.Insert(0, new ListItem("ALL", ""));
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void BindGridview()
    {
        string selectedVal = ddlGroupType.SelectedValue;
        string sqlQuery = "select * from  T_SMBPUtilityComm";

        if (!string.IsNullOrEmpty(selectedVal))
        {
            sqlQuery = "select * from  T_SMBPUtilityComm where Group_Type='" + selectedVal + "'";
        }

        SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, con);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        utility_grid.DataSource = dt;
        utility_grid.DataBind();
    }

    protected void ddlGroupType_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtMinAmount.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(txtMaxAmount.Text.Trim()))
                {
                    if (!string.IsNullOrEmpty(txtCharges.Text.Trim()))
                    {
                        SqlCommand cmd = new SqlCommand("sp_Insert_UpdateSMBPUtilityComm", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("Group_Type", (!string.IsNullOrEmpty(ddlGroupType.SelectedValue) ? ddlGroupType.SelectedValue : "all"));
                        cmd.Parameters.AddWithValue("Service", (!string.IsNullOrEmpty(ddlUtilityService.SelectedValue) ? ddlUtilityService.SelectedValue : "all"));
                        cmd.Parameters.AddWithValue("OperatorName", (!string.IsNullOrEmpty(ddlOperator.SelectedValue) ? ddlOperator.SelectedItem.Text : string.Empty));
                        cmd.Parameters.AddWithValue("SpKey", (!string.IsNullOrEmpty(ddlOperator.SelectedValue) ? ddlOperator.SelectedValue : "all"));
                        cmd.Parameters.AddWithValue("Charges_Type", ddlChargeType.SelectedValue);
                        cmd.Parameters.AddWithValue("MinAmount", txtMinAmount.Text.Trim());
                        cmd.Parameters.AddWithValue("MaxAmount", txtMaxAmount.Text.Trim());
                        cmd.Parameters.AddWithValue("Charges", txtCharges.Text.Trim());
                        cmd.Parameters.AddWithValue("type", "insert");

                        con.Open();
                        int k = cmd.ExecuteNonQuery();

                        if (k > 0)
                        {
                            ddlGroupType.SelectedIndex = 0;
                            ddlUtilityService.SelectedIndex = 0;
                            ddlOperator.SelectedIndex = 0;
                            ddlChargeType.SelectedIndex = 0;
                            txtMinAmount.Text = "0.0";
                            txtMaxAmount.Text = "0.0";
                            txtCharges.Text = "";

                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record submited successfully.');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record already Exists.');", true);
                        }
                        con.Close();
                        BindGridview();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter charges !');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter max. amount !');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter min. amount !');", true);
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void ddlUtilityService_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string selectedVal = ddlUtilityService.SelectedValue;

            ddlOperator.Items.Clear();

            ddlOperator.DataSource = STDom.GetSMBP_BBPSBillerList(selectedVal, "operator").Tables[0];
            ddlOperator.DataTextField = "Operator";
            ddlOperator.DataValueField = "SpKey";
            ddlOperator.DataBind();
            ddlOperator.Items.Insert(0, new ListItem("ALL", ""));
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void utility_grid_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            utility_grid.EditIndex = -1;
            BindGridview();

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void utility_grid_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            utility_grid.EditIndex = e.NewEditIndex;
            BindGridview();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void utility_grid_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            Label lblSNo = (Label)(utility_grid.Rows[e.RowIndex].FindControl("lblId"));
            int Id = Convert.ToInt32(lblSNo.Text.Trim().ToString());
            TextBox txtMinAMt = (TextBox)utility_grid.Rows[e.RowIndex].FindControl("txtModifyMinAmount");
            TextBox txtMaxAmt = (TextBox)utility_grid.Rows[e.RowIndex].FindControl("txtModifyMaxAmount");
            TextBox txtCharge = (TextBox)utility_grid.Rows[e.RowIndex].FindControl("txtModifyCharges");


            SqlCommand cmd = new SqlCommand("sp_Insert_UpdateSMBPUtilityComm", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Id", Id);
            cmd.Parameters.AddWithValue("MinAmount", txtMinAMt.Text.Trim());
            cmd.Parameters.AddWithValue("MaxAmount", txtMaxAmt.Text.Trim());
            cmd.Parameters.AddWithValue("Charges", txtCharge.Text.Trim());
            cmd.Parameters.AddWithValue("type", "update");

            con.Open();
            int k = cmd.ExecuteNonQuery();

            utility_grid.EditIndex = -1;
            BindGridview();

            if (k > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully updated.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('tryagain.');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('" + ex.Message + "');", true);
        }
    }

    protected void utility_grid_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int flag = 0;
            try
            {
                Label lblSNo = (Label)(utility_grid.Rows[e.RowIndex].FindControl("lblId"));
                int Id = Convert.ToInt32(lblSNo.Text.Trim().ToString());
                SqlCommand cmd = new SqlCommand("sp_Insert_UpdateSMBPUtilityComm", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@type", "delete");
                if (con.State == ConnectionState.Closed)
                    con.Open();
                flag = cmd.ExecuteNonQuery();
                con.Close();

            }
            catch (SqlException ex)
            {
                con.Close();
                clsErrorLog.LogInfo(ex);
            }


            if (flag > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Problen in deleting record.');", true);
            }

            BindGridview();
        }
        catch (Exception ex)
        {
            //ErrorLogTrace.WriteErrorLog(ex, "Flight")
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void utility_grid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string item = e.Row.Cells[0].Text;
            foreach (Button button in e.Row.Cells[2].Controls.OfType<Button>())
            {
                if (button.CommandName == "Delete")
                {
                    button.Attributes["onclick"] = "if(!confirm('Do you want to delete?')){ return false; };";
                }
            }
        }
    }

    protected void utility_grid_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        utility_grid.PageIndex = e.NewPageIndex;
        this.BindGridview();
    }
}