﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class SprReports_HoldPNR_ConfirmHoldTicket : System.Web.UI.Page
{
    string UserId = "";
    string HoldStatus = "";
    decimal AgentCreditLimit = 0;
    decimal TotalAfterDis = 0;
    string userType = "";
    string agencyInfo = "";
    SqlTransaction objDA = new SqlTransaction();
    SqlTransactionDom objSqlDom = new SqlTransactionDom();


    //
    private IntlDetails ID = new IntlDetails();
    private SqlTransaction ST = new SqlTransaction();
    private SqlTransactionDom STDom = new SqlTransactionDom();
    //private SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString);
    private SqlDataAdapter adap;
    private SqlTransactionNew objSql = new SqlTransactionNew();
    // Private objPG As New PaymentGateway

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            btnSubmit.Visible = false;
            btnReject.Visible = false;
            lblMsg.Visible = true;
            if (Session["UID"] != null && Convert.ToString(Session["UID"]) != "")
            {
                if (!string.IsNullOrEmpty(Request.QueryString["OrderId"]) && !string.IsNullOrEmpty(Request.QueryString["AgentId"]))
                {
                    string TrackId = Request.QueryString["OrderId"];
                    string UserId = Request.QueryString["AgentId"];

                    if (!IsPostBack)
                    {
                        #region GetRecorde
                        DataSet AgencyDs = objDA.GetAgencyDetails(UserId);
                        DataSet FltHdrDs = objDA.GetHdrDetails(TrackId);
                        //DataSet FltDs = objDA.GetFltDtls(OBTrackId, Session["UID"]);
                        //DataSet PaxDs = objDA.GetPaxDetails(OBTrackId);                    
                        //DataSet FltFareDs = objDA.GetFltFareDtl(OBTrackId);
                        //DataSet ds = GetBookingDetails(TrackId, UserId);

                        if (AgencyDs != null && AgencyDs.Tables[0].Rows.Count > 0)
                        {
                            lblAgentId.Text = Convert.ToString(AgencyDs.Tables[0].Rows[0]["User_Id"]);
                            if (!string.IsNullOrEmpty(Convert.ToString(AgencyDs.Tables[0].Rows[0]["Crd_Limit"])))
                            {
                                AgentCreditLimit = Convert.ToDecimal(AgencyDs.Tables[0].Rows[0]["Crd_Limit"]);
                            }
                            userType = Convert.ToString(AgencyDs.Tables[0].Rows[0]["Status"]);

                            agencyInfo += "<span style='font-family: arial, Helvetica, sans-serif;font-size:26px;font-weight:bold;color:#000'>" + AgencyDs.Tables[0].Rows[0]["Agency_Name"] + "(" + Convert.ToString(AgencyDs.Tables[0].Rows[0]["AgencyId"]) + ")</span><br/>";
                            //agencyInfo += "<span style='font-family: arial, Helvetica, sans-serif;font-size:26px;font-weight:bold;color:#000'>" + AgencyDs.Tables[0].Rows[0]["Agency_Name"] + "</span><br/>";
                            agencyInfo += "<span style='font-family: arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;  color: #666666'> " + AgencyDs.Tables[0].Rows[0]["Address"] + "</span>";
                            agencyInfo += "<span style='font-family: arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;  color: #666666'>, " + AgencyDs.Tables[0].Rows[0]["City"] + "</span>";
                            agencyInfo += "<span style='font-family: arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;  color: #666666'> - " + AgencyDs.Tables[0].Rows[0]["zipcode"] + "</span>";
                            agencyInfo += "<span style='font-family: arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;  color: #666666'>, " + AgencyDs.Tables[0].Rows[0]["State"] + "</span>";
                            agencyInfo += "<span style='font-family: arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;  color: #666666'><br/>Mob. " + AgencyDs.Tables[0].Rows[0]["Mobile"] + "</span>";
                            agencyInfo += "<span style='font-family: arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;  color: #666666'>,Email: " + AgencyDs.Tables[0].Rows[0]["Email"] + "</span>";


                            lbl_AgencyDetails.Text = agencyInfo.ToString();
                            lblCurrentBal.Text = AgencyDs.Tables[0].Rows[0]["Crd_Limit"].ToString();

                        }

                        if (FltHdrDs != null && FltHdrDs.Tables[0].Rows.Count > 0)
                        {
                            lblAmount.Text = Convert.ToString(FltHdrDs.Tables[0].Rows[0]["TotalAfterDis"]);
                            lblAgentId.Text = Convert.ToString(FltHdrDs.Tables[0].Rows[0]["AgentId"]);
                            lblSector.Text = Convert.ToString(FltHdrDs.Tables[0].Rows[0]["sector"]);
                            lblOrderId.Text = Convert.ToString(FltHdrDs.Tables[0].Rows[0]["OrderId"]);
                            if(Convert.ToString(FltHdrDs.Tables[0].Rows[0]["Status"]).ToLower()== "confirmbyagent")
                            {
                                lblStatus.Text = "Hold By Agent";
                            }
                           else if (Convert.ToString(FltHdrDs.Tables[0].Rows[0]["Status"]).ToLower() == "confirm")
                            {
                                lblStatus.Text = "Hold";
                            }
                            else
                            {
                                lblStatus.Text = Convert.ToString(FltHdrDs.Tables[0].Rows[0]["Status"]);
                            }
                            
                            HoldStatus = Convert.ToString(FltHdrDs.Tables[0].Rows[0]["Status"]);
                            if (!string.IsNullOrEmpty(Convert.ToString(FltHdrDs.Tables[0].Rows[0]["TotalAfterDis"])))
                            {
                                TotalAfterDis = Convert.ToDecimal(FltHdrDs.Tables[0].Rows[0]["TotalAfterDis"]);//ConfirmByAgent
                            }
                        }
                        if (userType == "TA" && TotalAfterDis > 0 && AgentCreditLimit > 0 && AgentCreditLimit >= TotalAfterDis && HoldStatus.ToLower() == "confirmbyagent" && Convert.ToString(FltHdrDs.Tables[0].Rows[0]["AgentId"]) == Convert.ToString(AgencyDs.Tables[0].Rows[0]["User_Id"]))
                        {
                            lblMsg.Text = " Amount " + TotalAfterDis + " wil be debited from your wallet to issue the ticket(s)";
                            btnSubmit.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                        {
                            btnSubmit.Visible = false;
                            btnReject.Visible = true;
                            if (HoldStatus.ToLower() == "confirm")
                            {
                                btnReject.Visible = false;
                                lblMsg.Visible = true;
                                lblMsg.Text = "The request has been already submitted,Please check in hold booking report";
                            }
                           else if (AgentCreditLimit < TotalAfterDis && HoldStatus.ToLower() == "confirmbyagent")
                            {
                                lblMsg.Text = "Agent wallet balance low.Please top-up agent wallet.";
                                btnSubmit.Visible = false;
                                btnReject.Visible = true;
                            }
                            else if (HoldStatus.ToLower() == "confirmbyagent" && Convert.ToString(FltHdrDs.Tables[0].Rows[0]["AgentId"]) == Convert.ToString(AgencyDs.Tables[0].Rows[0]["User_Id"]))
                            {
                                btnReject.Visible = true;
                            }
                        }
                        #endregion
                    }
                }
                else
                {

                }
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                Response.Redirect("Login.aspx?reason=Session TimeOut", false);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["UID"] != null && Convert.ToString(Session["UID"]) != "" && lblAgentId.Text != "")
            {
                #region Check Balance and Deduct
                DataSet AgencyDs = objDA.GetAgencyDetails(lblAgentId.Text);
                DataSet FltHdrDs = objDA.GetHdrDetails(lblOrderId.Text);
                if (Convert.ToString(FltHdrDs.Tables[0].Rows[0]["Status"]).Trim().ToLower() == "confirmbyagent")
                {
                    //if (Convert.ToString(AgencyDs.Tables[0].Rows[0]["Agent_Status"]).Trim() != "NOT ACTIVE" && Convert.ToString(AgencyDs.Tables[0].Rows[0]["Online_tkt"]).Trim() != "NOT ACTIVE")
                    //{
                    if (Convert.ToDouble(FltHdrDs.Tables[0].Rows[0]["TotalAfterDis"]) <= Convert.ToDouble(AgencyDs.Tables[0].Rows[0]["Crd_Limit"]))
                    {
                        double AvalBal = Convert.ToDouble(AgencyDs.Tables[0].Rows[0]["Crd_Limit"]) - Convert.ToDouble(FltHdrDs.Tables[0].Rows[0]["TotalAfterDis"]);

                        int Result = 0;
                        Result = objSqlDom.Ledgerandcreditlimit_Transaction(lblAgentId.Text.Trim(), Convert.ToDouble(FltHdrDs.Tables[0].Rows[0]["TotalAfterDis"]),
                            lblOrderId.Text, Convert.ToString(FltHdrDs.Tables[0].Rows[0]["VC"]), Convert.ToString(FltHdrDs.Tables[0].Rows[0]["GdsPnr"]),
                            Convert.ToString(AgencyDs.Tables[0].Rows[0]["Agency_Name"]), Request.UserHostAddress.ToString(), "", Convert.ToString(Session["UID"]), "",
                        AvalBal, "");
                        if (Result == 1)
                        {
                            //UpdateBookingStatus(OrderId,ExecutiveID, Status,Remark,RejectRemark,ActionType)
                            int flag = UpdateBookingStatus(lblOrderId.Text, Convert.ToString(Session["UID"]), "InProcess", txt_Remark.Text, "", "InProcess");
                           // int flag = UpdateBookingStatus(lblOrderId.Text, Convert.ToString(Session["UID"]), "Confirm", txt_Remark.Text, "", "Confirm");
                           // int flag = UpdateBookingStatus(lblOrderId.Text);
                            if (flag > 0)
                            {
                                lblMsg.Visible = true;
                                lblMsg.Text = "Please check hold booking report and update.";
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "key", "MyFunc()", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again.');", true);
                            }

                        }
                    }
                    else
                    {
                        lblMsg.Text = "Agent wallet balance low.Please top-up agent wallet.";
                        btnSubmit.Visible = false;
                        btnReject.Visible = true;
                    }
                    // }
                }

                #endregion
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    public int UpdateBookingStatus(string OrderId,string ExecutiveID, string Status, string Remark, string RejectRemark, string ActionType)
    {
        int temp = 0;
        SqlCommand cmd = new SqlCommand();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        try
        {
            con.Open();
            //cmd = new SqlCommand("USP_Update_StatusHold", con);//SP_Update_HoldBookingStatus
            cmd = new SqlCommand("SP_Update_HoldBookingStatus", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OderId", OrderId);
            cmd.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
            cmd.Parameters.AddWithValue("@PNRStatus", Status);  //@RejectedRemark  
            cmd.Parameters.AddWithValue("@RejectRemark", RejectRemark);
            cmd.Parameters.AddWithValue("@Remark", Remark);
            cmd.Parameters.AddWithValue("@ActionType", ActionType);
           
            temp = cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            //
        }
        finally
        {
            con.Close();
            cmd.Dispose();
        }
        return temp;
    }

    protected void btnReject_Click(object sender, EventArgs e)
    {

        //int flag = objDA.RejectHoldPNRStatusAndAmount(OrderID, TotalRefundAmt, Convert.ToString(dtID.Rows[0]["AgentId"]), Convert.ToString(dtID.Rows[0]["AgencyName"]), Convert.ToString(Session["UID"]), dtID.Rows[0]["GdsPnr"].ToString(), "Rejected", txt_Remark.Text, "Hold PNR Rejected Against  OrderID=" + OrderID, " Rejection", Request.UserHostAddress);
        try
        {
            if (Session["UID"] != null && Convert.ToString(Session["UID"]) != "" && lblAgentId.Text != "")
            {
                #region Check Balance and Deduct
               // DataSet AgencyDs = objDA.GetAgencyDetails(lblAgentId.Text);
                DataSet FltHdrDs = objDA.GetHdrDetails(lblOrderId.Text);
                if (Convert.ToString(FltHdrDs.Tables[0].Rows[0]["Status"]).Trim().ToLower() == "confirmbyagent")
                {

                    //UpdateBookingStatus(OrderId,ExecutiveID, Status,Remark,RejectRemark,ActionType)
                    int flag = UpdateBookingStatus(lblOrderId.Text, Convert.ToString(Session["UID"]), "Rejected", txt_Remark.Text,"Hold PNR Rejected Against  OrderID=" + lblOrderId.Text, "Rejected");                    
                    if (flag > 0)
                    {
                        lblMsg.Visible = true;
                        lblMsg.Text = "Hold PNR Rejected Against  OrderID=" + lblOrderId.Text;
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "key", "RejectPnr()", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again.');", true);
                    }
                }

                #endregion
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }


   
}