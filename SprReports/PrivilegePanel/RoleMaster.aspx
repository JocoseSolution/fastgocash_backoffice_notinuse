﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="RoleMaster.aspx.cs" Inherits="SprReports_PrivilegePanel_RoleMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-10">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Application Role</h3>
                    </div>
                    <div class="panel-body">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Role :</label>
                                <asp:TextBox CssClass="form-control" runat="server" ID="Roletxt" MaxLength="50" name="Role"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RFVMK" runat="server" ControlToValidate="Roletxt" ErrorMessage="*"
                                    Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                            </div>


                            <div class="form-group">
                                <label for="exampleInputEmail1">Role_Type :</label>
                                <asp:DropDownList CssClass="form-control" ID="RoleType" runat="server">
                                    <asp:ListItem Text="Executive" Value="EXEC"></asp:ListItem>
                                    <asp:ListItem Text="Agent" Value="AGENT"></asp:ListItem>
                                    <asp:ListItem Text="Admin" Value="ADMIN"></asp:ListItem>
                                    <asp:ListItem Text="Sales" Value="SALES"></asp:ListItem>
                                    <asp:ListItem Text="Account" Value="ACC"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-8"></div>
                            <div class="col-md-2">
                                <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="button buttonBlue" OnClick="Submit_Click" />
                            </div>
                        </div>




                        <div class="form-group">
                            <label for="exampleInputPassword1" id="Label1" runat="server"></label>

                        </div>






                        <div class="col-md-12">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>

                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Role_id"
                                        OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
                                        OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" PageSize="8"
                                        CssClass="table" GridLines="None" Width="100%">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Role" ItemStyle-Width="150">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtRole" runat="server" Text='<%# Eval("Role") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Role_Type" ItemStyle-Width="150">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRole_type" runat="server" Text='<%# Eval("Role_Type") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true" ItemStyle-Width="150" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

        $(document).ready(function () {

            $('#<%=Submit.ClientID%>').click(function (event) {

                if ($.trim($("#<%=RoleType.ClientID%>").val()) == "0") {
                    alert("value should be selected");
                    $("#<%=RoleType.ClientID%>").focus();
                      return false;
                  }


            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {

            $('#ContentPlaceHolder1_Submit').click(function (event) {

                var data = $("#ContentPlaceHolder1_Roletxt").val();


                var length = data.length;

                if (length < 1) {

                    $('.error').show();
                    return false;
                }
                else {
                    $('.error').hide();
                }


            });
        });
    </script>

    <script type="text/javascript">

        $(document).ready(function () {

            $('#<%=Submit.ClientID%>').click(function (event) {

                if ($.trim($("#<%=RoleType.ClientID%>").val()) == "0") {
                    alert("value should be selected");
                    $("#<%=RoleType.ClientID%>").focus();
                    return false;
                }


            });
        });
    </script>
</asp:Content>

