﻿Imports System.Data

Imports System.Configuration.ConfigurationManager
Partial Class SprReports_Hotel_HoldHotelRequest
    Inherits System.Web.UI.Page

    Private SqlTrans As New HotelDAL.HotelDA()
    Private STDom As New SqlTransactionDom
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Try
            If Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx", False)
            End If

            If Not IsPostBack Then
                If Session("User_Type") = "EXEC" Then
                    BindGrid()
                End If
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Protected Sub lnkAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lb As LinkButton = CType(sender, LinkButton)
            Dim gvr As GridViewRow = TryCast(lb.Parent.Parent, GridViewRow)
            gvr.BackColor = System.Drawing.ColorTranslator.FromHtml("#F0F8FF")
            'Update Modify Status in HtlHeader Table after Accept
            Dim i As Integer = SqlTrans.RejectHoldBooking(StatusClass.InProcess.ToString(), lb.CommandArgument, 0, Session("UID").ToString(), "")
            If i > 0 Then
                Page.ClientScript.RegisterStartupScript(GetType(Page), "MessagePopUp", "alert('Accept succesfully'); ", True)
                BindGrid()
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Protected Sub btnRemark_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemark.Click
        Dim STDom As New SqlTransactionDom()
        Dim SqlT As New SqlTransaction()
        Dim HoldGrdds As New DataSet()
        Try
            HoldGrdds = Session("HoldHotelRequestDS")
            For Each strow In HoldGrdds.Tables(0).Rows
                If strow.Item("OrderID").ToString() = Request("OrderIDS").Trim() Then
                    Dim Refundfare As Decimal = Convert.ToDecimal(strow.Item("NetCost"))
                    'Update Header table  After Reject
                    Dim i As Integer = SqlTrans.RejectHoldBooking(StatusClass.Rejected.ToString(), Request("OrderIDS").ToString(), Refundfare, Session("UID").ToString(), Request("txtRemark").ToString())
                    If i > 0 Then
                        If HoldGrdds IsNot Nothing Then
                            If HoldGrdds.Tables.Count > 0 Then
                                If HoldGrdds.Tables(0).Rows.Count > 0 Then
                                    RefundAmountAndLadgerEntry(Request("OrderIDS"), Refundfare, strow.Item("AgentID").ToString(), "", strow.Item("Provider"), strow.Item("HotelName").ToString(), strow.Item("AgencyName").ToString(), strow.Item("TripType").ToString())
                                    Page.ClientScript.RegisterStartupScript(GetType(Page), "MessagePopUp", "alert('Reject Successfully.'" & "); ", True)
                                    BindGrid()
                                    Try
                                        ''''Send Mail and SMS Start
                                        Dim HtlDetailsDs As New DataSet()
                                        HtlDetailsDs = SqlTrans.htlintsummary(Request("OrderIDS"), "Ticket")

                                        Dim objmail As New HotelBAL.HotelSendMail_Log()
                                        ''objmail.SendEmailForCancelAndReject(Request("OrderIDS"), HtlDetailsDs.Tables(0).Rows(0)("BookingID").ToString(), strow.Item("HotelName").ToString(), HtlDetailsDs.Tables(0).Rows(0)("LoginID").ToString(), HotelShared.HotelStatus.HOTEL_REJECT.ToSting(), StatusClass.Rejected.ToString())

                                        Dim objSMSAPI As New SMSAPI.SMS
                                        Dim objSqlNew As New SqlTransactionNew
                                        Dim smstext As String = ""
                                        Dim SmsCrd As DataTable
                                        Dim objDA As New SqlTransaction
                                        SmsCrd = objDA.SmsCredential(SMS.HOTELBOOKING.ToString()).Tables(0)
                                        Dim smsStatus As String = ""
                                        Dim smsMsg As String = ""
                                        If SmsCrd.Rows.Count > 0 AndAlso SmsCrd.Rows(0)("Status") = True Then
                                            smsStatus = objSMSAPI.SendHotelSms(Request("OrderIDS"), "", HtlDetailsDs.Tables(1).Rows(0)("GPhoneNo").ToString().Trim(), strow.Item("HotelName").ToString(), "", "", "Reject", smstext, SmsCrd)
                                            objSqlNew.SmsLogDetails(Request("OrderIDS"), HtlDetailsDs.Tables(1).Rows(0)("GPhoneNo").ToString().Trim(), smstext, smsStatus)
                                        End If
                                        ''''Send Mail and SMS End
                                    Catch ex As Exception
                                        HotelDAL.HotelDA.InsertHotelErrorLog(ex, " Send SMS Support Hold Request")
                                    End Try
                                End If
                            End If
                        End If
                        Exit For
                    Else
                        Page.ClientScript.RegisterStartupScript(GetType(Page), "MessagePopUp", "alert('Try later." & "); ", True)
                    End If
                End If
            Next
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Private Sub BindGrid()
        Try
            Dim HoldHotelRequestDS As New DataSet()
            HoldHotelRequestDS = SqlTrans.GetHoldHotel("", StatusClass.Hold.ToString(), "")
            HoldHotelRequestGrd.DataSource = HoldHotelRequestDS
            HoldHotelRequestGrd.DataBind()
            Session("HoldHotelRequestDS") = HoldHotelRequestDS
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub


    Protected Function RefundAmountAndLadgerEntry(ByVal orderid As String, ByVal RefundAmount As Decimal, ByVal AgentID As String, ByVal BookingID As String, ByVal Provider As String, ByVal HotelName As String, ByVal AgencyName As String, ByVal TripType As String) As Double
        Dim ablBalance As Double = 0
        Dim SqlT As New SqlTransaction()
        Try
            ablBalance = SqlT.UpdateNew_RegsRefund(AgentID, RefundAmount)
            Dim Result As Integer = STDom.LedgerEntry_Common(orderid, 0, RefundAmount, ablBalance, Provider, HotelName, BookingID, "HTLRFND", AgentID, AgencyName, Session("UID").ToString(), _
                                     Request.UserHostAddress.ToString(), "", "", "", "Hold Hotel Reject against " + orderid, TripType.Substring(0, 1), 0)
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
        Return ablBalance
    End Function

End Class
