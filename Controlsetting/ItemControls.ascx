﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ItemControls.ascx.cs" Inherits="UserControl_ItemControls" %>
<style type="text/css">
    .auto-style1 {
        width: 100%;
    }

    .auto-style2 {
        height: 40px;
    }

    .auto-style3 {
        height: 22px;
    }
</style>
<script type="text/javascript">
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if ((charCode >= 48 && charCode <= 57) || (charCode == 8)) {
            return true;
        }
        else {
            return false;
        }
    }
</script>
<script type="text/javascript">
    function ValidateFunction() {
      <%--  if (document.getElementById("<%=txt_id.ClientID%>").value == "") {
            alert('ID can not be blank');
            document.getElementById("<%=txt_id.ClientID%>").focus();
            return false;
        }--%>
        if (document.getElementById("<%=txt_name.ClientID%>").value == "") {
            alert('Name can not be blank');
            document.getElementById("<%=txt_name.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txt_width.ClientID%>").value == "") {
            alert('Width can not be blank');
            document.getElementById("<%=txt_width.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txt_height.ClientID%>").value == "") {
            alert('Height can not be blank');
            document.getElementById("<%=txt_height.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txt_displaytime.ClientID%>").value == "") {
            alert('Display time can not be blank');
            document.getElementById("<%=txt_displaytime.ClientID%>").focus();
            return false;
        }
    }
</script>
<table class="auto-style1">
   <%-- <tr>
        <td class="auto-style2">Control ID:
            <asp:TextBox ID="txt_id" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server"></asp:TextBox>
        </td>
    </tr>--%>
    <tr>
        <td>Control Name :
            <asp:TextBox ID="txt_name" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>Width :
            <asp:TextBox ID="txt_width" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>Height :<asp:TextBox ID="txt_height" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="auto-style3">Item Display At Time :<asp:TextBox ID="txt_displaytime" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="btn_submit" runat="server" OnClick="btn_submit_Click" OnClientClick="return ValidateFunction();" Text="Submit" />
        </td>
    </tr>
</table>
