﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Notification_Form.aspx.cs" Inherits="Notification_Form" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  
</head>
<body>

    <style type="text/css">
        .C_Message{
                height: 70px;
                width: 364px;
        }

    </style>
     <script src="Scripts/jquery-1.4.4.min.js"></script>
        <link href="CSS/jquery-ui-1.8.8.custom.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui-1.8.8.custom.min.js"></script>
    <script src="Scripts/AgencySearch.js"></script>
       <script type="text/javascript">
           var UrlBase = '<%=ResolveUrl("~/")%>';
           //var ApplUrl = 'http://b2b.//.com/';
    </script>
    <script>
        $(document).ready(function () {
            var hh = this;
            this.Form = $(".STARTDATEG");
            this.To = $(".ENDDATEG");

            var fFromDate = hh.Form.val();
            var tToDate = hh.To.val();

            //var returnDate = h.hidtxtRetDate.val();
            //Date Picker Bind

            var dtPickerOptions = {
                numberOfMonths: 1, dateFormat: "dd-mm-yy", maxDate: "+1y", minDate: "-2y", showOtherMonths: true, selectOtherMonths: false
            };
            if (hh.Form.length != 0) {
                hh.Form.datepicker(dtPickerOptions).datepicker("option", { onSelect: hh.UpdateRoundTripMininumDate }).datepicker("setDate", fFromDate.substr(0, 10));
            }
            if (hh.To.length != 0) {
                hh.To.datepicker(dtPickerOptions).datepicker("option", { onSelect: hh.UpdateRoundTripMininumDate }).datepicker("setDate", tToDate.substr(0, 10));
            }
        });

    </script>

   
    <form id="form1" runat="server">
         <a class="navbar-brand" href="<%= ResolveUrl("~/Dashboard.aspx")%>"><img src="<%= ResolveUrl("Images/logo.png") %>" alt="Header image" border="0" /><br />Back to Home</a>
        <%--<span><a style="color:black; text-align:right" href="<%= ResolveUrl("~/Dashboard.aspx")%>">Back to Home</a></span>--%>
    <div>
       

           <table cellspacing="10" cellpadding="10" border="0"  class="tbltbl" width="100%">
         
            <tr>
                
                <td  height="20px">
                    <h2 style="background-color: #004b91;color:white;text-align: center;">Notification</h2>
                </td>
               
            </tr>
            <tr>
                
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="width: 5%">
                            </td>
                            <td style="width: 90%">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td height="35px" class="Text">
                                            Tittle :*
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_Tittle"   placeholder="Tittle" class="form-controlaa" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Text" height="35px">
                                            Message :*
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_Message" runat="server" placeholder="Put Your Massage" class="form-controlaa" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Text" height="35px">
                                            UserType :*
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dd_UserType" placeholder="UserType" onchange = "selectChanged(this.value)" class="form-controlaa" runat="server"></asp:DropDownList>
                                    </tr>
                                    <tr>
                                        <td class="Text" height="35px">
                                              SpecialType:*
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DD_SPECIALType"  defvalue="Optional"  placeholder="Specail Type" class="form-controlaa" runat="server"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Text" height="35px">
                                            Agent ID :
                                        </td>
                                        <td>
                                            <div class="form-groups" id="td_Agency" runat="server">
                                               <input type="text" class="form-controlaa" id="txtAgencyName" runat="server" placeholder="Agency Name or ID" name="txtAgencyName"
                                           defvalue="Agency Name or ID" onfocus="focusObj(this);" onblur="blurObj(this);" autocomplete="off" />
                                           <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Text" height="35px">
                                            DisplayType :*
                                        </td>
                                        <td colspan="3">
                                             <asp:DropDownList  ID="DD_DISTYPE" runat="server"  class="form-controlaa">
                                                 <asp:ListItem Text="Select DisplayType" Value="0" Selected="true"></asp:ListItem>
                                                <asp:ListItem Text="marquee" Value="marquee"></asp:ListItem>                                             
                                                 <asp:ListItem Text="PopUP" Value="PopUP"></asp:ListItem>
         
                                             </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Text" height="35px">
                                          PageName:
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="PageName"  placeholder="Optional" class="form-controlaa"  runat="server"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="Text" height="35px">
                                          Start Date:*
                                        </td>
                                        <td colspan="3">
                                          <div class="form-groups">
                                          <input type="text" name="From" id="From" runat="server" placeholder="Start Date" class="form-controlaa" readonly="readonly" />
                                          </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="Text" height="35px">
                                          Start Time:*
                                        </td>
                                        <td>
                                          <div class="form-groups">
                                         <asp:DropDownList ID="DDStartTimeHH" placeholder="Start Hours" class="form-controlaa" runat="server"></asp:DropDownList>
                                         <asp:DropDownList ID="DDStartTimeMM" placeholder="Start Min" class="form-controlaa" runat="server"></asp:DropDownList>
                                          </div>
                                        </td>
                                    </tr>
                                       <tr>
                                        <td class="Text" height="35px">
                                          End Date:*
                                        </td>
                                        <td colspan="3">
                                        <div class="form-groups">
                                       <input type="text" name="To" placeholder="End Date" runat="server" id="To" class="form-controlaa" readonly="readonly" />
                                         </div>
                                        </td>
                                    </tr>
                                      <tr>
                                        <td class="Text" height="35px">
                                          End Time:*
                                        </td>
                                        <td>
                                          <div class="form-groups">
                                         <asp:DropDownList ID="DDEndTimeHH" placeholder="End Hours" class="form-controlaa" runat="server"></asp:DropDownList>
                                         <asp:DropDownList ID="DDEndTimeMM" placeholder="End Min" class="form-controlaa" runat="server"></asp:DropDownList>
                                          </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btn_Submit" runat="server" Text="Submit" Height="35px" Width="100px" Style="margin-right:20px;"
                                                OnClientClick="return Validate()" OnClick="btn_Submit_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 5%">
                            </td>
                        </tr>
                    </table>
                </td>
               
            </tr>
            
        </table>

        <div style="clear:both"></div>

                     <asp:GridView ID="gvDetails" DataKeyNames="ID" runat="server"  Width="100%"
                    AutoGenerateColumns="false" CssClass="Gridview" HeaderStyle-BackColor="#61A6F8"
                    ShowFooter="true" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White"
                    AllowPaging="true"   OnPageIndexChanging="gvDetails_PageIndexChanging" PageSize="30"
                    OnRowCancelingEdit="gvDetails_RowCancelingEdit" OnRowDeleted="gvDetails_RowDeleted" OnRowEditing="gvDetails_RowEditing" OnRowDeleting="gvDetails_RowDeleting"
                     OnRowUpdating="gvDetails_RowUpdating" OnRowCommand="gvDetails_RowCommand" OnRowDataBound="gvDetails_RowDataBound">      
                    <Columns>
                    <asp:TemplateField>
                    <EditItemTemplate>                 
                   <asp:Button ID="btnUpdate" CommandName="Update" runat="server" Text="update"  OnClientClick="return G_validate()"  ToolTip="Update" />
                   <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Cancel"  />
                    </EditItemTemplate>
                    <ItemTemplate>
                    <asp:Button ID="btnEdit" CommandName="Edit" Text="Edit" runat="server"  />
                    <asp:Button ID="btnDelete" CommandName="Delete" Text="Delete" runat="server" />
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Title">
                    <EditItemTemplate>
                    <asp:TextBox ID="Gtxt_tittle" TextMode="MultiLine" CssClass="C_tittle" runat="server" Text='<%#Eval("Title") %>'/>
                    </EditItemTemplate>
                    <ItemTemplate>
                     <asp:Label ID="Glbl_Title"  runat="server" Text='<%#Eval("Title") %>'/>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Message">
                    <EditItemTemplate>
                    <asp:TextBox ID="Gtxt_message" TextMode="MultiLine" CssClass="C_Message" runat="server" Text='<%#Eval("Message") %>'/>
                    </EditItemTemplate>
                    <ItemTemplate>
                    <asp:Label ID="Glbl_message" runat="server" Text='<%#Eval("Message") %>'/>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="UserType">
                    <EditItemTemplate>
                   <asp:Label ID="Elbl_userType" runat="server" Text='<%#Eval("UserType") %>'/>
                    </EditItemTemplate>
                    <ItemTemplate>
                    <asp:Label ID="lbl_userType" runat="server" Text='<%#Eval("UserType") %>'/>
                    </ItemTemplate>      
                    </asp:TemplateField>

                         <asp:TemplateField HeaderText="SpecialType">
                    <EditItemTemplate>
                   <asp:Label ID="Elbl_st" runat="server" Text='<%#Eval("SpecialType") %>'/>
                    </EditItemTemplate>
                    <ItemTemplate>
                    <asp:Label ID="lbl_st" runat="server" Text='<%#Eval("SpecialType") %>'/>
                    </ItemTemplate>      
                    </asp:TemplateField>


                         <asp:TemplateField HeaderText="UserID">
                    <EditItemTemplate>
                   <asp:Label ID="Elbl_Name" runat="server" Text='<%#Eval("Name") %>'/>
                    </EditItemTemplate>
                    <ItemTemplate>
                    <asp:Label ID="lbl_Name" runat="server" Text='<%#Eval("Name") %>'/>
                    </ItemTemplate>      
                    </asp:TemplateField>


                           <asp:TemplateField HeaderText="PageNamae">
                    <EditItemTemplate>
                   <asp:TextBox ID="Elbl_PGName" CssClass="C_pageName" runat="server" Text='<%#Eval("PageName") %>'/>
                    </EditItemTemplate>
                    <ItemTemplate>
                    <asp:Label ID="lbl_PGName" runat="server" Text='<%#Eval("PageName") %>'/>
                    </ItemTemplate>      
                    </asp:TemplateField>

                         <asp:TemplateField HeaderText="Display Type">
                    <EditItemTemplate>
                   <asp:Label ID="Elbl_TypeOfmsg" Visible="false" runat="server" Text='<%#Eval("TypeOfmsg") %>'/>
                         <asp:DropDownList  ID="GGDD_DISTYPE" runat="server"  class="form-controlaa">
                                                <asp:ListItem Text="marquee" Value="marquee"></asp:ListItem>                                              
                                                 <asp:ListItem Text="PopUP" Value="PopUP"></asp:ListItem>                                               
                       </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                    <asp:Label ID="lbl_TypeOfmsg" runat="server" Text='<%#Eval("TypeOfmsg") %>'/>
                    </ItemTemplate>      
                    </asp:TemplateField>
                  

                    <asp:TemplateField HeaderText="StartDate">
                    <EditItemTemplate>
                    <asp:TextBox ID="Gtxt_StartDate" CssClass="STARTDATEG" runat="server" Text='<%#Eval("StartDateN") %>'/>
                    </EditItemTemplate>
                    <ItemTemplate>
                    <asp:Label ID="Glbl_StartDate"  runat="server" Text='<%#Eval("StartDateN") %>'/>
                    </ItemTemplate>      
                    </asp:TemplateField>

                          <asp:TemplateField HeaderText="EndDate">
                    <EditItemTemplate>
                    <asp:TextBox ID="Gtxt_endDate"  CssClass="ENDDATEG" runat="server" Text='<%#Eval("EndDateN") %>'/>
                    </EditItemTemplate>
                    <ItemTemplate>
                    <asp:Label ID="Glbl_endDate" runat="server" Text='<%#Eval("EndDateN") %>'/>
                    </ItemTemplate>      
                    </asp:TemplateField>
                  
                    <asp:TemplateField HeaderText="startTime">
                    <EditItemTemplate>
                         <asp:Label ID="st_hours" Visible="false" runat="server" Text='<%#Eval("StartHour")%>'/>
                         <asp:Label ID="st_min" Visible="false" runat="server" Text='<%#Eval("startmin") %>'/>
                         <asp:DropDownList ID="GDDStartTimeHH" CssClass="C_GDDStartTimeHH" placeholder="Start Hours" class="form-controlaa" runat="server"></asp:DropDownList>
                         <asp:DropDownList ID="GDDStartTimeMM" CssClass="C_GDDStartTimeMM"  placeholder="Start Min" class="form-controlaa" runat="server"></asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                     <asp:Label ID="st_hours" Visible="false" runat="server" Text='<%#Eval("StartHour")%>'/>
                     <asp:Label ID="st_min" Visible="false" runat="server" Text='<%#Eval("startmin") %>'/>
                    <asp:Label ID="lbl_startTime" runat="server" Text='<%#Eval("startTime") %>'/>
                    </ItemTemplate>      
                    </asp:TemplateField>

                 
                    <asp:TemplateField HeaderText="EndTime">
                    <EditItemTemplate>
                         <asp:Label ID="end_hours" Visible="false" runat="server" Text='<%#Eval("EndHour") %>'/>
                         <asp:Label ID="end_min" Visible="false" runat="server" Text='<%#Eval("Endmin") %>'/>
                 <asp:DropDownList ID="GDDEndTimeHH" CssClass="C_GDDEndTimeHH"  placeholder="End Hours" class="form-controlaa" runat="server"></asp:DropDownList>
                 <asp:DropDownList ID="GDDEndTimeMM" CssClass="C_GDDEndTimeMM" placeholder="End Min" class="form-controlaa" runat="server"></asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                    <asp:Label ID="lbl_EndTime" runat="server" Text='<%#Eval("EndTime") %>'/>
                    </ItemTemplate>      
                    </asp:TemplateField>

                    </Columns>
                    </asp:GridView>  
    </div>
    </form>
    <script>
        function Validate() {
            if (document.getElementById("txt_Tittle").value == "") {
                alert('Please Enter Tittle');
                document.getElementById("txt_Tittle").focus();
                return false;
            }
            if (document.getElementById("txt_Message").value == "") {
                alert('Please Put the Massage Inside box');
                document.getElementById("txt_Message").focus();
                return false;
            }

            if (document.getElementById("dd_UserType").value.toUpperCase() == "Agent".toUpperCase()) {
                if (document.getElementById("DD_SPECIALType").value == "") {
                    alert('Please Select SpecailType');
                    document.getElementById("DD_SPECIALType").focus();
                    return false;
                }
            }

            if (document.getElementById("From").value == "") {
                alert('Please Select Start Date');
                document.getElementById("From").focus();
                return false;
            }

            if (document.getElementById("To").value == "") {
                alert('Please Select End Date');
                document.getElementById("To").focus();
                return false;
            }

            if (document.getElementById("dd_UserType").value == "0") {
                alert('Please Select UserType');
                document.getElementById("dd_UserType").focus();
                return false;
            }

            if (document.getElementById("DDStartTimeHH").value == "0") {
                alert('Please Select StartHour');
                document.getElementById("DDStartTimeHH").focus();
                return false;

            }
            if (document.getElementById("DDStartTimeMM").value == "0") {
                alert('Please Select StartMin');
                document.getElementById("DDStartTimeMM").focus();
                return false;

            }

            if (document.getElementById("DDEndTimeHH").value == "0") {
                alert('Please Select EndHour');
                document.getElementById("DDEndTimeHH").focus();
                return false;

            }
            if (document.getElementById("DDEndTimeMM").value == "0") {
                alert('Please Select EndMin');
                document.getElementById("DDEndTimeMM").focus();
                return false;

            }
            if (document.getElementById("DD_DISTYPE").value == "0") {
                alert('Please Enter Display Type');
                document.getElementById("DD_DISTYPE").focus();
                return false;
            }

        }

    </script>


     <script>
         function selectChanged(newvalue) {

             if (newvalue.toUpperCase() != "AGENT") {
                 $("#DD_SPECIALType").hide()
                 $("#txtAgencyName").hide()
             }
             else {
                 $("#DD_SPECIALType").show()
                 $("#txtAgencyName").show()

             }

         }



         function G_validate() {
             if ($(".C_tittle").val() == "") {
                 alert('Please Enter Tittle');
                 return false;
             }
             if ($(".C_Message").val() == "") {
                 alert('Please Put the Massage Inside box');
                 return false;
             }

             if ($(".C_GDDStartTimeHH").val() == "0") {
                 alert('Please SelectHour');
                 return false;
             }

             if ($(".C_GDDStartTimeMM").val() == "0") {
                 alert('Please SelectMin');
                 return false;
             }

             if ($(".C_GDDEndTimeHH").val() == "0") {
                 alert('Please Select EndHour');
                 return false;
             }

             if ($(".C_GDDEndTimeMM").val() == "0") {
                 alert('Please Select EndMin');
                 return false;
             }


             //if (document.getElementById("DD_SPECIALType").value == "0") {
             //    alert('Please Select SpecailType');
             //    document.getElementById("DD_SPECIALType").focus();
             //    return false;

             //}
             //if (document.getElementById("DDStartTimeHH").value == "0") {
             //    alert('Please Select StartHour');
             //    document.getElementById("DDStartTimeHH").focus();
             //    return false;

             //}
             //if (document.getElementById("DDStartTimeMM").value == "0") {
             //    alert('Please Select StartMin');
             //    document.getElementById("DDStartTimeMM").focus();
             //    return false;

             //}

             //if (document.getElementById("DDEndTimeHH").value == "0") {
             //    alert('Please Select EndHour');
             //    document.getElementById("DDEndTimeHH").focus();
             //    return false;

             //}
             //if (document.getElementById("DDEndTimeMM").value == "0") {
             //    alert('Please Select EndMin');
             //    document.getElementById("DDEndTimeMM").focus();
             //    return false;

             //}





             //if (document.getElementById("td_Agency").value == "") {
             //    alert('Please Enter AgencyID');
             //    document.getElementById("td_Agency").focus();
             //    return false;
             //}

             //if (document.getElementById("DD_DISTYPE").value == "0") {
             //    alert('Please Enter Display Type');
             //    document.getElementById("DD_DISTYPE").focus();
             //    return false;
             //}

         }

    </script>
    
</body>
</html>
